
import cornell_pix as cp

import argparse
import math
import numpy as np
import random

from PIL import Image
from PIL import ImageDraw
from PIL import ImageEnhance
from PIL import ImageFilter
from PIL import ImageOps
from PIL import ImageStat

def mask_0( image, mean, multiple ):
    (width, height) = image.size

    modified = np.array( [0] * (height * width), dtype = np.ubyte )
    modified = modified.reshape( height, width )

    max_trials = multiple * width * height
    trial = 0
    while trial < max_trials:
        radius = -mean * math.log( random.random() )
        angle = random.random() * 2.0 * math.pi

        x = radius * math.cos( angle )
        y = radius * math.sin( angle )

        column = width // 2 + int(x * width / 2)
        row = height // 2 + int(y * height / 2)

        if 0 <= column < width and 0 <= row < height:
            modified[row, column] = 255

        trial = trial + 1

    return Image.fromarray( modified, mode = 'L' )
# end of mask_0()

def neighborhood_average( i, j, original, neighborhood_size, t ):
    (height, width, depth) = original.shape

    bc = np.array( [0,0,0], dtype = int )

    bc = bc + original[i, j]

    # east
    x_offset = int(t * random.random() * neighborhood_size)
    y = i
    x = j + x_offset
    x = x if x < width else (width - 1)
    next = original[y, x]
    bc = bc + next

    # west
    x_offset = int(t * random.random() * neighborhood_size)
    y = i
    x = j - x_offset
    x = x if x >= 0 else 0
    next = original[y, x]
    bc = bc + next

    # north
    y_offset = int(t * random.random() * neighborhood_size)
    y = i + y_offset
    y = y if y < height else (height - 1)
    x = j
    next = original[y, x]
    bc = bc + next

    # south
    y_offset = int(t * random.random() * neighborhood_size)
    y = i - y_offset
    y = y if y >= 0 else 0
    x = j
    next = original[y, x]
    bc = bc + next

    # northeast
    x_offset = int(t * random.random() * neighborhood_size)
    y_offset = int(t * random.random() * neighborhood_size)
    x = j + x_offset
    x = x if x < width else (width - 1)
    y = i - y_offset
    y = y if y >= 0 else 0
    next = original[y, x]
    bc = bc + next

    # northwest
    x_offset = int(t * random.random() * neighborhood_size)
    y_offset = int(t * random.random() * neighborhood_size)
    x = j - x_offset
    x = x if x >= 0 else 0
    y = i - y_offset
    y = y if y >= 0 else 0
    next = original[y, x]
    bc = bc + next

    # southwest
    x_offset = int(t * random.random() * neighborhood_size)
    y_offset = int(t * random.random() * neighborhood_size)
    x = j - x_offset
    x = x if x >= 0 else 0
    y = i + y_offset
    y = y if y < height else (height - 1)
    next = original[y, x]
    bc = bc + next

    # southeast
    x_offset = int(t * random.random() * neighborhood_size)
    y_offset = int(t * random.random() * neighborhood_size)
    x = j + x_offset
    x = x if x < width else (width - 1)
    y = i + y_offset
    y = y if y < height else (height - 1)
    next = original[y, x]
    bc = bc + next

    bc = bc // 9

    return bc
# end of neighborhood_average()

def filter_1( original, clear_radius, neighborhood_size ):
    (height, width, depth) = original.shape

    modified = np.array( [0] * (height * width * depth), dtype = int )
    modified = modified.reshape( height, width, depth )

    r = clear_radius
    for i in range( height ):
        for j in range( width ):
            (radius, angle) = cp.normalized_polar_coordinates( i, j, width, height)
            if radius > r:
                t = (radius - r)/(1.0 - r)

                bc = neighborhood_average( i, j, original, neighborhood_size, t )

                modified[i, j] = cp.weighted_average_colors( original[i,j], bc, math.sqrt(t) )
            else:
                modified[i, j] = original[i,j]

    return modified
# end of filter_1()

def filter_2( image ):
    mean = 0.2
    multiple = 0.8
    mask = mask_0( image, mean, multiple)

    #mask.show()

    neighborhood_size = 12
    blurred = image.filter( ImageFilter.ModeFilter( neighborhood_size ) )
    enhancer = ImageEnhance.Brightness( blurred )
    blurred = enhancer.enhance( 1.6 )

    #blurred.show()

    result = Image.composite( image, blurred, mask )
    return result
# end of filter_2()

def filter_3( image ):
    original = np.asarray( image )

    clear_radius = 0.2
    neighborhood_size = 32

    modified = filter_1( original, clear_radius, neighborhood_size )

    image = Image.fromarray( np.uint8(modified), mode = 'RGB' )
    #image = ImageOps.grayscale( image )

    enhancer = ImageEnhance.Color( image )
    image = enhancer.enhance( 2.0 )

    return image
# end of filter_3()

def filter_4( image_a, image_b, image_c, left_fence, right_fence ):

    array_a = np.asarray( image_a )
    array_b = np.asarray( image_b )
    array_c = np.asarray( image_c )

    # dimensions of all 3 images must be the same
    (height, width, depth) = array_a.shape

    result = np.array( [0] * (height * width * depth), dtype = np.ubyte )
    result = result.reshape( height, width, depth )

    left_fence = int( left_fence * width )
    right_fence = int( right_fence * width )


    for row in range( height ):
        for column in range( width ):
            if column < left_fence:
                t = column / left_fence
                a = array_a[ row, column ].tolist()
                b = array_b[ row, column ].tolist()
                result[ row, column ] = cp.weighted_average_colors( a, b, abs(t) )
            elif left_fence <= column <= right_fence:
                result[ row, column ] = array_b[ row, column ].tolist()
            else:
                t = (column - right_fence) / (width - right_fence)
                b = array_b[ row, column ].tolist()
                c = array_c[ row, column ].tolist()
                result[ row, column ] = cp.weighted_average_colors( b, c, abs(t) )

    return Image.fromarray( result, mode = 'RGB' )
# end of filter_4()

def filter_5( image ):
    enhancer = ImageEnhance.Color( image )
    faded = enhancer.enhance( 0.2 )
    enhancer = ImageEnhance.Brightness( faded )
    faded = enhancer.enhance( 6.0 )

    contour = image.filter( ImageFilter.CONTOUR )
    faded_left = Image.blend( faded, contour, 0.5 )
    faded_right = Image.blend( faded, contour, 0.9 )

    # 0.52 and 0.75 for Tyler's picture

    image = filter_4( faded_left, image, faded_right, 0.32, 0.60 )

    return image
# end of filter_5()

def distance_between_colors( a, b ):
    delta_red = abs(int(a[0]) - int(b[0]))
    delta_green = abs(int(a[1]) - int(b[1]))
    delta_blue = abs(int(a[2]) - int(b[2]))

    return delta_red + delta_green + delta_blue
# end of distance_between_colors()

def closest_match( row, column, picture, spectrum ):
    index = 0
    a = picture[row, column]
    b = spectrum[index]

    distance = distance_between_colors( a, b )
            
    for i in range( 1, len(spectrum) ):
        b = spectrum[i]

        d = distance_between_colors( a, b )
        #print( 'distance = {:4d}'.format( d ), a, b )
        if d < distance:
            index = i
            distance = d

    #print( '\nindex = {:4d}\n\n'.format( index ) )

    return spectrum[ index ]
# end of closest_match()

def filter_6( image ):
    picture = np.asarray( image )
    (height, width, depth) = picture.shape

    result = np.array( [0] * (height * width * depth), dtype = np.ubyte )
    result = result.reshape( height, width, depth )

    spectrum = []
    for i in range(12):
        row = int( height/2 + random.random() * height/2 )
        column = int( width/2 + random.random() * width/2 )
        spectrum.append( picture[row, column ] )

    for row in range( height ):
        for column in range( width ):
            (hue, saturation, value) = cp.rgb_to_hsv( picture[row,column] )
            if 200 < hue < 320 or value > 0.9:
                result[row, column] = picture[row, column]
            else:
                result[row, column] = closest_match( row, column, picture, spectrum )

    return Image.fromarray( result, mode = 'RGB' )
# end of filter_6()

def filter_7( image ):
    picture = np.asarray( image )
    (height, width, depth) = picture.shape

    top = ImageOps.posterize( image, 2 )
    enhancer = ImageEnhance.Brightness( top )
    top = enhancer.enhance( 2.0 )
    top = top.filter( ImageFilter.BLUR )
    top = top.filter( ImageFilter.EDGE_ENHANCE )

    enhancer = ImageEnhance.Color( image )
    bottom = enhancer.enhance( 2.0 )
    bottom = bottom.filter( ImageFilter.SHARPEN )
    # bottom = Image.blend( bottom, bottom_edges, 0.9 )

    top_array = np.asarray( top )
    bottom_array = np.asarray( bottom )

    result = np.array( [0] * (height * width * depth), dtype = np.ubyte )
    result = result.reshape( height, width, depth )

    x0 = 0.0
    y0 = 0.5

    x1 = 0.2
    y1 = 0.7

    x2 = 0.6
    y2 = 0.1

    x3 = 1.0
    y3 = 0.3

    control_points = [(x0, y0), (x1, y1), (x2, y2), (x3, y3)]

    curve = cp.BezierCurve( control_points )

    for row in range( height ):
        for column in range( width ):
            distance = curve.distance(row, column, width, height)
            if  distance < 0.0:
                a = top_array[ row, column ].tolist()
                b = bottom_array[ row, column ].tolist()
                result[row, column] = cp.weighted_average_colors( b, a, -distance )
            else:
                # a = top_array[ row, column ].tolist()
                b = bottom_array[ row, column ].tolist()
                result[row, column] = b # cp.weighted_average_colors( b, a, distance )

    return Image.fromarray( result, mode = 'RGB' )
# end of filter_7()

def filter_8( image ):
    picture = np.asarray( image )
    (height, width, depth) = picture.shape


    result = np.array( [0] * (height * width * depth), dtype = np.ubyte )
    result = result.reshape( height, width, depth )

    # (0.700, 0.484) 
    control_points = ( (0.000, 1.00), (0.246, 1.00), (0.600, 0.484), (0.700, 0.484) )
    curve = cp.BezierCurve( control_points )

    for row in range( height ):
        for column in range( width ):
            color = picture[row, column]
            bw = cp.compute_shade_min_max_mean( color.tolist() )

            if column < 0.700 * width:
                distance = curve.distance( row, column, width, height )

                if distance > 0.0:
                    result[row, column] = cp.weighted_average_colors( color, bw, distance )
                else:
                    result[row, column] = bw 
            else:
                result[row, column] = color

    return Image.fromarray( result, mode = 'RGB' )
# end of filter_8()

def filter_9( image ):
    original = np.asarray( image )

    enhancer = ImageEnhance.Color( image )
    colorful = enhancer.enhance( 2.0 )
    enhancer = ImageEnhance.Brightness( colorful )
    bright = enhancer.enhance( 4.0 )
    blurred = bright.filter( ImageFilter.ModeFilter(18) )
    vivid = np.asarray( blurred )
    

    (height, width, depth) = vivid.shape

    result = np.array( [0] * (height * width * depth), dtype = np.ubyte )
    result = result.reshape( height, width, depth )

    p0 = (0.0, 0.6)
    p1 = (0.2, 0.8)
    p2 = (0.8, 0.4)
    p3 = (1.0, 0.1)

    control_points = (p0, p1, p2, p3)
    curve = cp.BezierCurve( control_points )

    for row in range( height ):
        for column in range( width ):
            t = curve.distance( row, column, width, height )
            #if t < 0:
            #    result[row, column] = vivid[row, column]
            #else:
            #    result[row,column] = original[row, column]

            a = original[row, column].tolist()
            b = vivid[row, column].tolist()
            color = cp.weighted_average_colors( a, b, abs(t) )
            result[row, column] = color

    return Image.fromarray( result, mode = 'RGB' )
# end of filter_9()

def filter_10( image ):
    original = np.asarray( image )

    (height, width, depth) = original.shape

    outline = np.asarray( image.filter( ImageFilter.CONTOUR ) )

    result = np.array( [0] * (height * width * depth), dtype = np.ubyte )
    result = result.reshape( height, width, depth )

    threshold = 180

    for row in range( height ):
        for column in range( width ):
            (r, g, b) = outline[row, column].tolist()
            if  r < threshold or g < threshold or b < threshold:

                color = original[row, column] // 2
                result[row, column] = color
                if column + 1 < width:
                    result[row, column + 1] = color
                if column - 1 >= 0:
                    result[row, column - 1] = color
                if row + 1 < height:
                    result[row + 1, column] = color
                if row - 1 >= 0:
                    result[row - 1, column] = color
                if column + 1 < width and row + 1 < height:
                    result[row + 1, column + 1] = color
                if column - 1 >= 0 and row - 1 >= 0:
                    result[row - 1, column - 1] = color
                if column + 1 < width and row - 1 >= 0:
                    result[row - 1, column + 1] = color
                if column - 1 >= 0 and row + 1 < height:
                    result[row + 1, column - 1] = color
            else:
                result[row, column] = (255, 255, 255)

    cartoon = Image.fromarray( result, mode = 'RGB' )
    background = image.filter( ImageFilter.ModeFilter(12) )
    enhancer = ImageEnhance.Brightness( background )
    background = enhancer.enhance( 3.0 )
    return Image.blend( cartoon, background, 0.4 )
# end of filter_10()

def main( arguments ):
    image = Image.open( arguments.image_file )
    image = cp.make_golden_rectangle( image )
    image = cp.halve_size_of_image( image )

    # image = filter_2( image ) / cole-library.jpg
    # image = filter_3( image ) / old-and-new.jpg
    # image = filter_5( image ) / tyler-carrington.jpg
    # image = filter_6( image ) / chapel-clouds.jpg
    # image = filter_7( image ) / norton-flowers.jpg
    # image = filter_8( image ) / west-facade.jpg
    # image = filter_9( image ) / audi-fabrik.jpg

    image = filter_3( image )

    image.show()
    image.save( 'gallery/output.jpg' )
    image.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument( 'image_file', type = str, help = 'Image file' )
    parser.add_argument( '--fraction', type = float, default = 1.0,
        help = 'Fraction' )

    arguments = parser.parse_args()

    main( arguments )
