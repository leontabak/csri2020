
import cornell_pix as cp

import argparse
import math

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFilter
from PIL import ImageOps

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
# end of Point constructor

    def id(self):
        # multiplier to shift decimal point 4 spaces to the right
        ten_thousand = 10000

        prefix = int( round( ten_thousand * self.x ) )
        suffix = int( round( ten_thousand * self.y ) )
        return ten_thousand * prefix + suffix
# end of Point class' id() method

    def map(self, world_a, world_b):
        # get coordinates of the points that define
        # the first coordinate system
        a_min_x = world_a.upper_left.x
        a_max_y = world_a.upper_left.y

        a_max_x = world_a.lower_right.x
        a_min_y = world_a.lower_right.y

        # normalized coordinates
        nx = (self.x - a_min_x) / (a_max_x - a_min_x)
        ny = (self.y - a_min_y) / (a_max_y - a_min_y)
        

        b_min_x = world_b.upper_left.x
        b_min_y = world_b.lower_right.y

        b_max_x = world_b.lower_right.x
        b_max_y = world_b.upper_left.y

        width = b_max_x - b_min_x
        height = b_max_y - b_min_y

        x = b_min_x + width * nx
        y = b_min_y + height * ny

        return (x, y)
# end of Point class' map() method

# end of Point class

class PointBank:
    def __init__(self):
        self.centers = dict()
        self.vertices = dict()
# end of PointBank class' constructor

    def get_center( self, x, y ):
        p = Point(x, y)
        key = p.id()
        if not key in self.centers:
            self.centers[key] = p            
        return self.centers[key]
# end of PointBank class' get_center() method

    def get_vertex( self, x, y ):
        p = Point(x, y)
        key = p.id()
        if not key in self.vertices:
            self.vertices[key] = p
        return self.vertices[key]
# end of PointBank class' get_vertex method

# end of PointBank class

class Rectangle:
    def __init__(self, upper_left, lower_right):
        self.upper_left = upper_left
        self.lower_right = lower_right
# end of Rectangle class' constructor

    def in_bounds(self, point):
        min_x = self.upper_left.x
        min_y = self.lower_right.y

        max_x = self.lower_right.x
        max_y = self.upper_left.y

        x = point.x
        y = point.y

        x_in_bounds = min_x <= x <= max_x
        y_in_bounds = min_y <= y <= max_y

        return x_in_bounds and y_in_bounds
# end of Rectangle class' in_bounds() method

# end of Rectangle class

class Hexagon:
    def __init__(self, bank, center, radius, phase = 0):
        self.bank = bank
        self.center = center
        self.outer_radius = radius
        self.inner_radius = radius * math.cos( math.pi/6 )
        self.phase = phase
        self.vertices = []
        self.shared_vertex_neighbors = []
        self.shared_edge_neighbors = []

        # compute the coordinates of the hexagon's vertices

        vertices = []
        for n in range( 0, 6 ):
            angle = n * math.pi / 3        
            x = center.x + radius * math.cos (angle + phase )
            y = center.y + radius * math.sin (angle + phase )
            v = self.bank.get_vertex( x, y )
            vertices.append( v )
        self.vertices = vertices

        # compute coordinates of centers of hexagons that
        # share a vertex with this hexagon
        shared_vertex_neighbors = []
        angle = phase
        for n in range( 0, 6 ):
            x = center.x + 2.0 * self.outer_radius * math.cos( angle )
            y = center.y + 2.0 * self.outer_radius * math.sin( angle )
            p = self.bank.get_center(x, y)
            shared_vertex_neighbors.append( p )
            angle += math.pi / 3
        self.shared_vertex_neighbors = shared_vertex_neighbors

        # compute coordinates of centers of hexagons that
        # share an edge with this hexagon
        shared_edge_neighbors = []
        angle = phase + math.pi / 6
        for n in range( 0, 6 ):
            x = center.x + 2.0 * self.inner_radius * math.cos( angle )
            y = center.y + 2.0 * self.inner_radius * math.sin( angle )
            p = self.bank.get_center(x, y)
            shared_edge_neighbors.append( p )
            angle += math.pi / 3
        self.shared_edge_neighbors = shared_edge_neighbors

# end of Hexagon class' constructor 

    def circumscribed_circle(self, world_a, world_b):
        # compute coordinates of smallest square that
        # encloses the hexagon

        ulx = self.center.x - self.outer_radius
        uly = self.center.y + self.outer_radius

        lrx = self.center.x + self.outer_radius
        lry = self.center.y - self.outer_radius

        upper_left = Point( ulx, uly)
        lower_right = Point( lrx, lry)

        upper_left = upper_left.map( world_a, world_b )
        lower_right = lower_right.map( world_a, world_b )

        (ulx, uly) = upper_left
        (lrx, lry) = lower_right

        ulx = int(ulx)
        uly = int(uly)

        lrx = int(lrx)
        lry = int(lry)

        return ((ulx, lry), (lrx, uly))

# end of Hexagon class' circumscribed_circle() method


    def inscribed_circle(self, world_a, world_b):
        ulx = self.center.x - self.inner_radius
        uly = self.center.y + self.inner_radius

        lrx = self.center.x + self.inner_radius
        lry = self.center.y - self.inner_radius

        upper_left = Point( ulx, uly)
        lower_right = Point( lrx, lry)

        upper_left = upper_left.map( world_a, world_b )
        lower_right = lower_right.map( world_a, world_b )

        (ulx, uly) = upper_left
        (lrx, lry) = lower_right

        ulx = int(ulx)
        uly = int(uly)

        lrx = int(lrx)
        lry = int(lry)

        return ((ulx, lry), (lrx, uly))

# end of Hexagon class' inscribed_circle() method

    def map_vertices(self, world_a, world_b):
        vertices = []
        for v in self.vertices:
            vertices.append( v.map(world_a, world_b) )
        return vertices

# end of Hexagon class' map_vertices() method

# end of Hexagon class

class Tiling:
    def __init__(self, bank, radius, phase, bounds):
        self.bank = bank
        self.radius = radius
        self.phase = phase
        self.bounds = bounds

# end of Tiling class

    def new_and_in_bounds(self, tile_center, centers):
        is_new = not tile_center.id() in centers
        is_in_bounds = self.bounds.in_bounds( tile_center )

        return is_new and is_in_bounds

# end of Tiling class' new_and_in_bounds() method

    def tiling(self):
        center_x = (self.bounds.lower_right.x - self.bounds.upper_left.x) / 2.0
        center_y = (self.bounds.upper_left.y - self.bounds.lower_right.y) / 2.0
        tile_center = self.bank.get_center( center_x, center_y )
        candidates = [ tile_center ]
        centers = set()
        tiles = []

        while candidates:

            tile_center = candidates.pop()

            if self.new_and_in_bounds( tile_center, centers ):
                new_tile = Hexagon(self.bank, tile_center, self.radius, self.phase) 

                candidates.extend( new_tile.shared_edge_neighbors )

                centers.add( tile_center.id() )
                tiles.append( new_tile )

        return tiles

# end of Tiling class' tiling() method

# end of Tiling class

def make_image_square( image ):
    (width, height) = image.size

    # crop to get a square image
    if width > height:
        margin = (width - height) // 2
        ulx = margin
        uly = 0
        lrx = width - margin
        lry = height
        image = image.crop( (ulx, uly, lrx, lry) )
    else:
        margin = (height - width) // 2
        ulx = 0
        uly = margin
        lrx = width
        lry = height - margin
        image = image.crop( (ulx, uly, lrx, lry) )

    (width, height) = image.size

    image = ImageOps.fit( image, (width, height) )

    return image

# end of make_image_square() function

def trim_image( image, radius ):
    (width, height) = image.size

    # print( 'Before trim: {:4d}, {:4d}'.format( width, height ) )

    ulx = radius * width
    uly = radius * height
    lrx = width - radius * width
    lry = height - radius * height

    image = image.crop( (ulx, uly, lrx, lry) )

    (width, height) = image.size

    image = ImageOps.fit( image, (width, height) )

    (width, height) = image.size

    # print( 'After trim: {:4d}, {:4d}'.format( width, height ) )

    return image

# end of trim_image() function

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument( 'image_file', type = str, help = 'Image file' )
    parser.add_argument( '--radius', type = float, default = 0.125, help = 'Size of cells' )

    args = parser.parse_args()


    radius = args.radius
    phase = 0.0

    photo = Image.open( args.image_file )
    photo = cp.make_golden_rectangle( photo )
    photo = cp.halve_size_of_image( photo )

    (width, height) = photo.size

    black_square = Image.new( mode = 'RGB', size = (width, height), color  = (0, 0, 0) )

    photo = photo.filter( ImageFilter.GaussianBlur( radius * width ) )

    device_bounds = Rectangle( Point(0, height), Point(width, 0) )

    canvas = ImageDraw.Draw( black_square )
    
    world_bounds = Rectangle( Point(0.0, height/width), Point( 1.0, 0.0) )

    bank = PointBank()

    tiling = Tiling( bank, radius, phase, world_bounds )
    tiles = tiling.tiling()

    draw_hexagons = True

    for t in tiles:
        (column, row) = t.center.map( world_bounds, device_bounds )
        column = int( column )
        row = int( row )
        color = photo.getpixel( (column, row) )

        vertices = t.map_vertices( world_bounds, device_bounds )

        if draw_hexagons:
            canvas.polygon( vertices, fill = color, outline = color )
        else:
            canvas.ellipse( t.inscribed_circle(world_bounds, device_bounds), fill = color, outline = color )
 
    black_square = trim_image( black_square, radius )

    black_square.save( 'gallery/output.png' )
    black_square.show()
    photo.close()

# end of main() function

if __name__ == '__main__':
    main()
