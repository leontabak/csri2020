
import argparse
import math

from PIL import Image
from PIL import ImageEnhance
from PIL import ImageFilter
from PIL import ImageOps

def main( parameters ):
    photo = Image.open( parameters.file_name )

    bw_photo = photo.copy()
    bw_photo = ImageOps.grayscale( bw_photo )
    bw_photo = bw_photo.convert( 'RGB')

    color_photo = photo.copy()
    color_photo = color_photo.filter( ImageFilter.EMBOSS )
    enhancer = ImageEnhance.Color( color_photo )
    enhancer.enhance( 8.0 )

    #bw_photo.show()
    #color_photo.show()

    (width, height) = photo.size
    mask = Image.new( size = (width, height), mode = 'L',
                      color = 255 )

    radius_of_circle = parameters.radius * min( width, height )

    center_x = width // 2
    center_y = height // 2

    for column in range( width ):
        for row in range( height ):
            dx = column - center_x
            dy = row - center_y
            distance = math.sqrt( dx * dx + dy * dy )
            if distance < radius_of_circle:
                mask.putpixel( (column, row), 0 )
            #else:
            #    mask.putpixel( (column, row), 255 )

    print( 'bw_photo size = ', bw_photo.size )
    print( 'color_photo size = ', color_photo.size )
    print( 'mask size = ', mask.size )

    print('bw_photo mode = ', bw_photo.mode)
    print('color_photo mode = ', color_photo.mode)
    print('mask mode = ', mask.mode)
    photo = Image.composite( bw_photo, color_photo, mask )

    photo.show()
    photo.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument( 'file_name', type = str, help = 'File name' )
    parser.add_argument( 'radius', type = float, help = 'Radius')
    arguments = parser.parse_args()
    main( arguments )
