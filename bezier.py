
from PIL import Image
from PIL import ImageDraw
from PIL import ImageEnhance
from PIL import ImageFilter
from PIL import ImageOps

import numpy as np

def bezier( control_points, t ):
    # a point on a Bezier curve can be computed by multiplying 3 matrices:
    #   (1) a matrix with 1 row and 4 columns
    #       The elements of this matrix are powers of a weight t (0.0 <= t <= 1.0).
    #       The value of t determines which point on the curve this function
    #       returns.
    #   (2) a matrix with 4 rows and 4 columns
    #       The elements of this matrix are always the same.
    #   (3) a matrix with 4 rows and 2 columns
    #       The elements of this matrix are the coordinates
    #       of 4 controls points.
    #       The choice of control points determines the shape
    #       of the curve.

    # A Bezier curve begins at the first control point and ends
    # at the fourth control point.
    # In general, it will not pass through the second and third
    # control points but will lie within a polygon (the "convex hull")
    # defined by all 4 control points.

    m = np.array( [[1, 0, 0, 0], [-3, 3, 0, 0], [3, -6, 3, 0], [-1, 3, -3, 1]] )

    t = np.array( [1, t, t * t, t * t * t] )
    t = t.reshape( 1, 4 )

    control_x = control_points[:, 0]
    control_x = control_x.reshape( 4, 1 )

    control_y = control_points[:, 1]
    control_y = control_y.reshape( 4, 1 )

    row_vector = t.dot( m )

    point = row_vector.dot( control_points )

    x = point[0,0]
    y = point[0,1]

    point = (x, y)

    return point
# end of bezier()

def map_endpoints( normalized_tail, normalized_head, 
        image_width, image_height ):

    (x0, y0) = normalized_tail
    (x1, y1) = normalized_head

    x0 = int( x0 * image_width )
    y0 = int( y0 * image_height )
    x1 = int( x1 * image_width )
    y1 = int( y1 * image_height )

    return (x0, y0, x1, y1)
# end of map_endpoints()

def draw_curve( image, control_points ):
    (image_width, image_height) = image.size

    draw = ImageDraw.Draw( image )

    # specify width of line in pixels
    line_width = 8

    # specify color of line
    line_color = (255, 255, 0)

    steps = 512
    previous_point = bezier( control_points, 0.0 )
    for n in range( 1, steps +  1):
        t = n / steps
        current_point = bezier( control_points, t )

        endpoints = map_endpoints( previous_point, current_point, 
            image_width, image_height )


        draw.line( endpoints, width = line_width, fill = line_color )

        previous_point = current_point
# end of draw_curve()


def main():

    # TO-DO: experiment with a different image
    image = Image.open( 'ink-pond-iris.jpg' )
    contours = image.filter( ImageFilter.CONTOUR )

    # TO-DO: experiment with different control points
    # 0.0 <= x0, x1, x2, x3 <= 1.0
    # x0 <= x1 <= x2 <= x3
    # 0.0 <= y0, y1, y2, y3 <= 1.0

    x0 = 0.00
    y0 = 0.50

    x1 = 0.25
    y1 = 1.40

    x2 = 0.75
    y2 = 0.20

    x3 = 1.00
    y3 = 0.50

    bound = np.array( [[x0, y0], [x1, y1], [x2, y2], [x3, y3]] )
    bound = bound.reshape( 4, 2 )

    # construct mask

    mask = Image.new( "L", image.size )

    (image_width, image_height) = image.size
    for row in range( 0, image_height ):
        for column in range( 0, image_width ):
            t = column / image_width
            point = bezier( bound, t)

            # map the normalized coordinate to the image
            y = point[1] * image_height

            # color pixels in the mask black if below the curve,
            # white if above the curve
            if row < y:
                mask.putpixel( (column, row), 0 )
            else:
                mask.putpixel( (column, row), 255 )

    # modify the two parts of the image
    # (the part that is covered by the mask and the part
    # that is not covered by the mask)
    # (or, to say this another way, the part that is
    # above the curve and the part that is below the curve)

    # TO-DO: experiment with a different filter
    bottom = image.filter( ImageFilter.CONTOUR )
    # TO-DO: experiment with different values of alpha
    # 0.0 <= alpha <= 1.0
    alpha = 0.0
    bottom = Image.blend( bottom, image, alpha )

    # TO-DO: experiment with a different filter
    top = image.filter( ImageFilter.DETAIL )
    color_enhancer = ImageEnhance.Color( top )

    # TO-DO: experiment with different values of factor
    # factor >= 0
    factor = 1.0
    top = color_enhancer.enhance( factor )

    # TO-DO: experiment with different values of bits
    # 0 < bits <= 8
    bits = 8
    top = ImageOps.posterize( top, bits )

    image = Image.composite( bottom, top, mask )

    #draw_curve( image, bound )

    image.show()

    image.close()
# end of main()

if __name__ == '__main__':
    main()
