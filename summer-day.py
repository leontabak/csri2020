
import argparse

from PIL import Image
from PIL import ImageFilter
from PIL import ImageOps

def make_image_square( image ):
    (width, height) = image.size

    # crop to get a square image
    if width > height:
        margin = (width - height) // 2
        ulx = margin
        uly = 0
        lrx = width - margin
        lry = height
        image = image.crop( (ulx, uly, lrx, lry) )
    else:
        margin = (height - width) // 2
        ulx = 0
        uly = margin
        lrx = width
        lry = height - margin
        image = image.crop( (ulx, uly, lrx, lry) )

    (width, height) = image.size

    image = ImageOps.fit( image, (width, height) )

    return image

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument( 'image_file', type = str, help = 'Image file' )

    parser.add_argument( '--size', type = int, default = 1,
        help = 'Integer parameter for filter' )

    parser.add_argument( '--alpha', type = float, default = 0.5,
        help = 'Floating point parameter for filter' )

    args = parser.parse_args()

    image = Image.open( args.image_file )

    image = make_image_square( image )

    (width, height) = image.size

    width = width // 2
    height = height // 2
    image = ImageOps.fit( image, (width, height) )

    pen_and_ink = image.filter( ImageFilter.CONTOUR )

    image = image.filter( ImageFilter.ModeFilter( args.size ) )

    image = Image.blend( pen_and_ink, image, args.alpha )

    image.show()
    image.close()

if __name__ == '__main__':
    main()
