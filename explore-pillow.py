
import math
from PIL import Image
from PIL import ImageFilter
import random

import cornell_pix as cp



def transform_pixel( photo, row, column, width, height ):
    original_color = photo.getpixel( (column, row) )
    return original_color

def transform_photo( photo ):
    (width, height) = photo.size

    copy = photo.copy()

    for column in range( width ):
        for row in range( height ):

            color = transform_pixel( photo, row, column, width, height )

            copy.putpixel( (column, row), color )

    return copy

    return photo

def posterize( intensity ):
    number_of_distinct_intensities = 2
    band_width = 256 // number_of_distinct_intensities
    return band_width * round(intensity / band_width)

def contrast( intensity ):
    distance = (intensity - 128) / 128

    if distance > 0:
        dx = 1.0 - distance
        dx = dx * dx
        #dx = math.exp( -dx )
        #dx = math.sqrt( dx )
        intensity = 128 + round( 128 * (1.0 - dx))
    else:
        dx = 1.0 + distance
        dx = dx * dx
        #dx = math.exp( -dx )
        #dx = math.sqrt( dx )
        intensity = 128 - round( 128 * (1.0 - dx))

    return intensity
 
def clip( intensity ):
    (low, middle, high) = (64, 128, 192)
    if middle < intensity <= high:
        return high
    elif low < intensity <= middle:
        return low
    else:
        return intensity

def main():
    original_photo = Image.open( "lilac-1024x768.jpg" )
    original_photo.show()

    (width, height) = original_photo.size
    print( "width = ", width )
    print( "height = ", height )

    changed_photo = Image.eval( original_photo, contrast )

    #changed_photo = original_photo.filter( ImageFilter.CONTOUR ) 
    #changed_photo = original_photo.filter( ImageFilter.DETAIL )
    #changed_photo = original_photo.filter( ImageFilter.BLUR )
    #for i in range( 12 ):
    #    changed_photo = changed_photo.filter( ImageFilter.BLUR )

    #changed_photo = original_photo.filter( ImageFilter.GaussianBlur(32) )

    changed_photo.show()

if __name__ == "__main__":
    main()
