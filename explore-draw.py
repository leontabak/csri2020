
import argparse

from PIL import Image
from PIL import ImageDraw

def main( arguments ):
    image = Image.open( arguments.image_file_name )

    draw = ImageDraw.Draw( image )

    (image_width, image_height) = image.size

    # specify coordinates of one end of the line
    x0 = 0
    y0 = image_height // 2

    # specify coordinates of the other end of the line
    x1 = image_width
    y1 = image_height // 2

    # specify width of line in pixels
    line_width = 8

    # specify color of line
    line_color = (255, 255, 0)

    draw.line( (x0, y0, x1, y1), width = line_width, fill = line_color )

    image.show()
    image.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument( 'image_file_name', type = str, 
        help = 'Name of image file' )

    arguments = parser.parse_args()

    main( arguments )
