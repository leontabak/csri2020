
import math
from PIL import Image

photo = Image.open( "lilac-1024x768.jpg" )
photo.show()

(width, height) = photo.size
print( "width = ", width )
print( "height = ", height )

def fun0( photo ):
    (width, height) = photo.size

    copy = photo.copy()

    for i in range( width ):
        for j in range( height ):
            (red, green, blue) = photo.getpixel( (i, j) )
    
            avg = (red + green + blue) // 3
    
            copy.putpixel( (i, j), (avg, avg, avg) )

    copy.show()
# end of fun0()

def fun1( photo ):
    (width, height) = photo.size

    copy = photo.copy()

    for i in range( width ):
        for j in range( height ):
            (red, green, blue) = photo.getpixel( (i, j) )

            minimum = min( red, green, blue )
            maximum = max( red, green, blue )

            avg = (minimum + maximum) // 2
    
            copy.putpixel( (i, j), (avg, avg, avg) )

    copy.show()
# end of fun1()

def fun2( photo ):
    (width, height) = photo.size

    copy = photo.copy()

    for i in range( width ):
        for j in range( height ):
            (red, green, blue) = photo.getpixel( (i, j) )

            minimum = min( red, green, blue )
            maximum = max( red, green, blue )

            avg = (minimum + maximum) // 2

            if i > width // 2:
                copy.putpixel( (i, j), (avg, avg, avg) )
            else:
                copy.putpixel( (i, j), (red, green, blue) )

    copy.show()
# end of fun2()

def fun3( photo ):
    (width, height) = photo.size

    copy = photo.copy()

    for i in range( width ):
        for j in range( height ):
            (red, green, blue) = photo.getpixel( (i, j) )

            minimum = min( red, green, blue )
            maximum = max( red, green, blue )

            avg = (minimum + maximum) // 2

            if i > j:
                copy.putpixel( (i, j), (avg, avg, avg) )
            else:
                copy.putpixel( (i, j), (red, green, blue) )

    copy.show()
# end of fun3()

def g( d ):
    #return d * d
    #return d * d * d
    #return math.exp( d )
    return math.sqrt( abs(d) )

def fun4( photo ):
    (width, height) = photo.size

    copy = photo.copy()

    numberOfPixels = width * height

    redSum = 0
    greenSum = 0
    blueSum = 0

    for i in range( width ):
        for j in range( height ):
            (red, green, blue) = photo.getpixel( (i, j) )

            redSum += red
            greenSum += green
            blueSum += blue

    redMean = redSum // numberOfPixels
    greenMean = greenSum // numberOfPixels
    blueMean = blueSum // numberOfPixels

    for i in range( width ):
        for j in range( height ):
            (red, green, blue) = photo.getpixel( (i, j) )

            redDistance = redMean - red
            greenDistance = greenMean - green
            blueDistance = blueMean - blue

            d = 0.0
            adjustedRed = 0
            adjustedGreen = 0
            adjustedBlue = 0

            if redDistance > 0:
                d = redDistance / redMean
                d = g(d)
                redAdjusted = redMean - int( d * redMean )
            else:
                d = redDistance / (255 - redMean)
                d = g(d)
                redAdjusted = redMean + int( d * (255 - redMean) )

            if greenDistance > 0:
                d = greenDistance / greenMean
                d = g(d)
                greenAdjusted = greenMean - int( d * greenMean )
            else:
                d = greenDistance / (255 - greenMean)
                d = g(d)
                greenAdjusted = greenMean + int( d * (255 - greenMean) )

            if blueDistance > 0:
                d = blueDistance / blueMean
                d = g(d)
                blueAdjusted = blueMean - int( d * blueMean )
            else:
                d = blueDistance / (255 - blueMean)
                d = g(d)
                blueAdjusted = blueMean + int( d * (255 - blueMean) )

            copy.putpixel( (i, j), (redAdjusted, greenAdjusted, blueAdjusted) )

    copy.show()
# end of fun4()

def fun5( photo ):
    (width, height) = photo.size

    copy = photo.copy()

    for i in range( width ):
        for j in range( height ):
            (red, green, blue) = photo.getpixel( (i, j) )

            minimum = min( red, green, blue )
            maximum = max( red, green, blue )

            avg = (minimum + maximum) // 2

            avg = (avg // 32)

            if avg == 0:
                copy.putpixel( (i, j), (180, 144, 248) )
            elif avg == 1:
                copy.putpixel( (i, j), (208, 152, 112) )
            elif avg == 2:
                copy.putpixel( (i, j), (236, 106, 116) )
            elif avg == 3:
                copy.putpixel( (i, j), (48, 160, 220) )
            elif avg == 4:
                copy.putpixel( (i, j), (0, 180, 255) )
            elif avg == 5:
                copy.putpixel( (i, j), (255, 255, 112) )
            elif avg == 6:
                copy.putpixel( (i, j), (163, 74, 227) )
            else:
                copy.putpixel( (i, j), (6, 143, 92) )


    copy.show()
# end of fun5()


def main():
    #fun0( photo )

    #fun1( photo )
            
    #fun2( photo )

    #fun3( photo )

    #fun4( photo )

    fun5( photo )

if __name__ == "__main__":
    main()

