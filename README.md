# Experimentation with Image Processing

## cornell_pix.py

This is a module (not an executable program). 
Other programs in this collection make use of this module.

* BezierCurve class
    - BezierCurve( control_points ) constructor
    - normalized_point( t ) method
    - point( column, width, height) method
    - distance( row, column, width, height ) method

* Point class
    - Point( x, y ) constructor
    - x instance variable
    - y instance variable

* is_approximately_equal( a, b ) function

* weighted_average( a, b, t ) function

* weighted_average_points( a, b, t ) function

* weighted_average_colors( a, b, t ) function

* rgb_to_hsv( color ) function

* hsv_to_rgb( color ) function

* make_spectrum( a, b, number_of_colors ) function

* normalized_cartesian_coordinates( row, column, width, height ) function

* normalized_polar_coordinates( row, column, width, height ) function

* compute_shade_arithmetic_mean( color ) function

* compute_shade_min_max_mean( color ) function

* compute_shade_weighted_average( color, weights ) function

* compute_shade_luminosity( color ) function

* compute_bw_negative( color ) function

* compute_color_negative( color ) function

* compute_shade_posterization( color, spectrum ) function

* make_image_square( image ) function

* halve_size_of_image( image ) function

* map_endpoints( normalized_tail, normalized_head,
  image_width, image_height ) function

* draw_curve( image, control_points ) function

## explore-black-white.py

  This program translates a color image to a black and white
  (grayscale) image using 3 different formulae.
  It displays the color and black and white images side by side.
  Vertical lines separate the four parts of the image.

  The program requires a single command line parameter.
  The value of this parameter must be the path of an image file.

  Run the program by typing:

`python3 explore-black-white.py images/walk-to-chapel.jpg`

## explore-blending.py

  This program produces an image in color in a circular region
  at the center and in shades of gray ("black and white") elsewhere.
  The transition from color to black and white is smooth.

  The program requires a command line parameter.
  The value of this parameter must be the path of an image file.

  The value of the optional *radius* parameter determines
  the size of the circular area.

  The optional *radius* parameter should be a number in the range
  0.0--1.0.

  Run the program by typing:

`python3 explore-blending.py images/walk-to-chapel.jpg --radius 0.4`

## explore-convolutions.py

  This program defines four 3 x 3 kernels for convolutions.
  These kernels produces these effects:
  
* sharpen (selected when kernel_index  = 0)
* outline (selected when kernel_index  = 1)
* blur (selected when kernel_index  = 2)
* emboss (selected when kernel_index  = 3)

  The program's structure allows easy addition of other
  kernels.

  The program requires a single command line parameter.
  The value of this parameter must be the path of an image file.

  An optional integer parameter (named *kernel_index*)
  determines which of the
  four kernels the program uses in filtering the given
  image. The default value of this parameter is 0 and corresponds
  to the kernel that sharpens the image.

  Run the program by typing:

`python3 explore-convolutions.py images/walk-to-chapel.jpg --kernel-index 0`

## explore-crop.py

  This program exercises the `crop()` and `paste()` methods
  of the `Image` class.

  It produces four smaller images by copying rectangular regions
  from the top, bottom, left, and right of a photograph.
  It applies the `ImageFilter.CONTOUR` filter to those
  four smaller images and then pastes them back into
  the original photograph in their original positions.
  The center of the photograph remains unaltered.

  The result is an image that features a portion of
  the original color photograph in a rectangular region
  at the center, surrounded by what looks like a pen-and-ink
  line drawing.

  The program requires a single command line parameter.
  The value of this parameter must be the path of an image file.

  Run the program by typing:

`python3 explore-crop.py images/lilac-1024x768.jpg`

## explore-draw.py

  This program draws a horizontal line across the
  middle of an image.

  It shows how to create an instance of the `Image.Draw` class
  and how to use the `line()` method of that class.

  (The `Image.Draw` class also provides methods for drawing
  other shapes and text.)

  The program calls the methods with parameters that specify:

* the coordinates of the line segment's endpoints
* the width (thickness) of the line segment
* the color of the line segment segment

  The program requires a single command line parameter.
  The value of this parameter must be the path of an image file.

  Run the program by typing:

`python3 explore-draw.py images/ink-pond-iris.jpg`

## explore-enhance.py

  This program applies a filter that enhances one
  of the following attributes of an image:

* brightness
* color
* contrast
* sharpness

  It does this by creating one of these classes:

* `ImageEnhance.Brightness` (selected when *index* = 0)
* `ImageEnhance.Color` (selected when *index* = 1)
* `ImageEnhance.Contrast` (selected when *index* = 2)
* `ImageEnhance.Sharpness` (selected when *index* = 3)

  It then calls the `enhance()` method of the class.
  The single parameter of this method is an `Image` object.

  The program displays the original photograph and the
  modified image in separate windows.

  The program requires a command line parameter.
  The value of this parameter must be the path of an image file.

  An optional integer command line parameter (named *index*)
  determines which kind of enhancer the program uses.
  The default value of this parameter is 0.

  An optional floating point command line parameter
  (named *factor*) determines the degree of enhancement.
  The value of this parameter must be non-negative.
  The default value is 1.0.
  With the default value, the program makes no change
  to the original image.

  Run the program by typing:

`python3 explore-enhance.py images/lilac-1024x768.jpg --index 0 --factor 2.0` 

## explore-filters.py

  This program applies one of several filters to an
  image. It does this by calling the `filter()` method
  of the `Image` class. The method's single parameter
  is one of these objects:

* `ImageFilter.BLUR` (selected when *index* = 0)
* `ImageFilter.CONTOUR` (selected when *index* = 1)
* `ImageFilter.DETAIL` (selected when *index* = 2)
* `ImageFilter.EDGE\_ENHANCE` (selected when *index* = 3)
* `ImageFilter.EDGE\_ENHANCE\_MORE` (selected when *index* = 4)
* `ImageFilter.EMBOSS` (selected when *index* = 5)
* `ImageFilter.FIND\_EDGES` (selected when *index* = 6)
* `ImageFilter.SHARPEN` (selected when *index* = 7)
* `ImageFilter.SMOOTH` (selected when *index* = 8)
* `ImageFilter.SMOOTH\_MORE` (selected when *index* = 9)

  The program displays both the original image and the
  modified image.

  The program requires a command line parameter.
  The value of this parameter must be the path of an image file.

  An optional integer command line parameter (named *index*)
  determines which filter the program applies. The default
  value of this parameter is 0.

  Run the program by typing:

`python3 explore-filters.py images/lilac-1024x768.jpg --index 0` 

## explore-ops.py

  This program produces a new image by applying
  one of these methods to a given image:

* `ImageOps.grayscale()` (selected when *operation* = 0)
* `ImageOps.invert()` (selected when *operation* = 1)
* `ImageOps.posterize()` (selected when *operation* = 2)
* `ImageOps.solarize()` (selected when *operation* = 3)

  The name of the image file is fixed in the source code.

  The program requires a command line parameter.
  The value of this parameter must be the path of an image file.

  An optional integer command line parameter (named *operation*)
  specifies which operation is applied.

  The program displays both the original image and the
  modified image.

  Run the program by typing:

`python3 explore-ops.py --operation 1` 

## explore-statistics.py

  This program computes and displays:

* the format of the image file
* the dimensions of the image
* statistical summaries of the values of the 
  red, green, and blue components of the pixels' colors
    - minima
    - maxima
    - means
    - medians
    - standard deviations

  The name of the image file is fixed in the source code.

  This program reads this image file:
  *images/lilac-1024x768.jpg*.

  Run the program by typing:

`python3 explore-statistics.py`

## explore-tiling.py

  This program produces an image of hexagonal tiles.
  The color of each tile is the average of the colors of the pixels
  in a corresponding part of a photograph.

  The value of the *radius* parameter determines the size
  of each hexagon (and hence the number of tiles).

  The optional *radius* parameter should be a number in the range
  0.0--1.0.

  Run the program by typing:

`python3 explore-tiling.py images/walk-to-chapel.jpg --radius 0.02`

## explore-3-effects.py

  This program produces an image that has 3 parts.
  There is a top part, middle part, and bottom part.
  The boundaries between parts are curved.

  The program applies different filters (or other
  modifications) to a photograph in each part.

  The default value of the optional *output_file_name*
  parameter is *output.jpg*.

  Run the program by typing:

`python3 explore-3-effects.py images/walk-to-chapel.jpg --size 12 
    --output_file_name output.jpg`


