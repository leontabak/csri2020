
import cornell_pix as cp

import argparse
import numpy as np

from PIL import Image
from PIL import ImageEnhance
from PIL import ImageFilter
from PIL import ImageOps



def main( arguments ):
    photo = Image.open( arguments.image_file_name )

    photo = cp.make_image_square( photo )
    photo = cp.halve_size_of_image( photo )

    (width, height) = photo.size

    original = np.asarray( photo )

    top_mask_array = np.array( [0] * (width * height), dtype = int )
    top_mask_array = top_mask_array.reshape( width, height )

    middle_mask_array = np.array( [0] * (width * height), dtype = int )
    middle_mask_array = middle_mask_array.reshape( width, height )

    bottom_mask_array = np.array( [0] * (width * height), dtype = int )
    bottom_mask_array = bottom_mask_array.reshape( width, height )

    print( 'shape of original = ', original.shape )
    print( 'shape of top mask = ', top_mask_array.shape )
    print( 'shape of middle mask = ', middle_mask_array.shape )
    print( 'shape of bottom mask = ', bottom_mask_array.shape )

    p0 = (0.00, 0.40)
    p1 = (0.40, 0.80)
    p2 = (0.80, 0.10)
    p3 = (1.00, 0.30)

    control_points_03 = (p0, p1, p2, p3)

    p4 = (0.00, 0.80)
    p5 = (0.40, 0.60)
    p6 = (0.80, 0.80)
    p7 = (1.00, 0.90)

    control_points_47 = (p4, p5, p6, p7)

    curve_03 = cp.BezierCurve( control_points_03 )
    curve_47 = cp.BezierCurve( control_points_47 )

    for i in range( original.shape[0] ):
        for j in range( original.shape[1] ):
            y_03 = curve_03.point( j, width, height ).y
            y_47 = curve_47.point( j, width, height ).y

            if i < y_03:
                top_mask_array[i,j] = 0
            else:
                top_mask_array[i,j] = 255
 
            if y_03 < i < y_47:
                middle_mask_array[i,j] = 0
            else:
                middle_mask_array[i, j] = 255

            if i > y_47:
                bottom_mask_array[i,j] = 0
            else:
                bottom_mask_array[i,j] = 255

    top_mask = Image.fromarray( np.uint8( top_mask_array ), mode = 'L' )
    middle_mask = Image.fromarray( np.uint8( middle_mask_array ), mode = 'L' )
    bottom_mask = Image.fromarray( np.uint8( bottom_mask_array ), mode = 'L' )

    top_photo = photo.filter( ImageFilter.CONTOUR )
    top_photo = top_photo.filter( ImageFilter.SHARPEN )
    enhancer = ImageEnhance.Contrast( top_photo )
    top_photo = enhancer.enhance( 50 )
    #top_photo = ImageOps.posterize( top_photo, 2 )

    middle_photo = ImageOps.grayscale( photo )
    bottom_photo = photo.filter( ImageFilter.ModeFilter(arguments.size) )

    print( 'top photo mode = ', top_photo.mode )
    print( 'middle photo mode = ', middle_photo.mode )
    print( 'bottom_photo_mode  =', bottom_photo.mode )

    photo = Image.composite( middle_photo, top_photo, top_mask )
    photo = Image.composite( photo, bottom_photo, bottom_mask )

    #cp.draw_curve( photo, control_points_03 )
    #cp.draw_curve( photo, control_points_47 )

    #top_mask.show()
    #middle_mask.show()
    #bottom_mask.show()


    photo.show()
    photo.save( arguments.output_file_name )
    photo.close()
# end of main()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument( 'image_file_name', type = str, 
        help = 'Name of image file' )
    parser.add_argument( '--size', type = int, default = 1,
        help = 'Size of neighborhood used by filter' )
    parser.add_argument( '--output_file_name', type = str, default = 'output.jpg',
        help = 'Name of output image file' )

    arguments = parser.parse_args()
   
    main( arguments )
