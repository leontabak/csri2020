
import math
from PIL import Image
import random

import cornell_pix as cp


def neighbors_coordinates( x, y, delta_x, delta_y, width, height ):
    x = (x + delta_x + width) % width
    y = (y + delta_y + height) % height
    return (x, y )

def blur( photo, row, column, width, height, radius ):
    x = column
    y = row

    dx = 0
    dy = random.randint( 0, radius )
    north = neighbors_coordinates( x, y, dx, dy, width, height )

    dx = -random.randint( 0, radius )
    dy = random.randint( 0, radius )
    northwest = neighbors_coordinates( x, y, dx, dy, width, height )

    dx = -random.randint( 0, radius )
    dy = 0
    west = neighbors_coordinates( x, y, dx, dy, width, height )

    dx = -random.randint( 0, radius )
    dy = -random.randint( 0, radius )
    southwest = neighbors_coordinates( x, y, dx, dy, width, height )

    dx = 0
    dy = -random.randint( 0, radius )
    south = neighbors_coordinates( x, y, dx, dy, width, height )

    dx = random.randint( 0, radius )
    dy = -random.randint( 0, radius )
    southeast = neighbors_coordinates( x, y, dx, dy, width, height )

    dx = random.randint( 0, radius )
    dy = 0
    east = neighbors_coordinates( x, y, dx, dy, width, height )

    dx = random.randint( 0, radius )
    dy = random.randint( 0, radius )
    northeast = neighbors_coordinates( x, y, dx, dy, width, height )

    neighbors = []
    neighbors.append( north )
    neighbors.append( northwest )
    neighbors.append( west )
    neighbors.append( southwest )
    neighbors.append( south )
    neighbors.append( southeast )
    neighbors.append( east )
    neighbors.append( northeast )

    red, green, blue = photo.getpixel( (column, row) )
    for n in neighbors:
        (x, y) = n
        r, g, b = photo.getpixel( (x, y) )
        red += r
        green += g
        blue += b

    red = red // 9
    green = green // 9
    blue = blue // 9

    return (red, green, blue)

def transform_pixel( photo, row, column, width, height ):
    original_color = photo.getpixel( (column, row) )

    (radius, angle) = cp.normalized_polar_coordinates( row, column, 
        width, height )

    boundary = 0.3

    if radius < boundary:
        bw_color = cp.compute_shade_luminosity( original_color )
        shade = bw_color[0]

        if shade < int(0.4 * 255):
            shade  = shade / 255
            shade = shade * shade * shade
            shade = int( shade * 255 )
            return (shade, shade, shade )
        else:
            return original_color
    else:
        weight = 1.0 - (1.0 - radius) / (1.0 - boundary)

        radius = int( 8 * weight )

        blurred_color = blur( photo, row, column, width, height, radius )
        bw_color = cp.compute_shade_luminosity( blurred_color )

        shade = bw_color[0]

        if shade < 0.4:
            shade = shade * shade * shade
            return (shade, shade, shade)
        else:
            changed_color = cp.weighted_average_colors( original_color,
                bw_color, weight )
            return changed_color

def transform_photo( photo ):
    (width, height) = photo.size

    copy = photo.copy()

    for column in range( width ):
        for row in range( height ):

            color = transform_pixel( photo, row, column, width, height )

            copy.putpixel( (column, row), color )

    return copy

    return photo

def main():
    original_photo = Image.open( "lilac-1024x768.jpg" )
    original_photo.show()

    (width, height) = original_photo.size
    print( "width = ", width )
    print( "height = ", height )

    changed_photo = transform_photo( original_photo )

    changed_photo.show()

if __name__ == "__main__":
    main()
