
import numpy as np

class BezierSurface:
    def __init__(self, z_values):
        cp = np.array( [0, 0, 0] * 16, dtype = np.float64 )
        cp = np.reshape( cp, (3, 4, 4) )
        print( 'shape = ', cp.shape )

        for i in range(0, 4):
            for j in range(0, 4):
                np.put( cp[:,i,j], range(3), (j/3, i/3, z_values[i,j]) )

        self.control_points = cp



    def print_control_points(self):
        cp = self.control_points

        for i in range(0, 4):
            for j in range(0, 4):
                print( '({:4.2f}, '.format( cp[0,i,j] ), end = '' )
                print( '{:4.2f}, '.format( cp[1,i,j] ), end = '' )
                print( '{:4.2f}) '.format( cp[2,i,j] ), end = '' )
            print( '\n' )

def main():
    print( 'Guten Tag!' )
    z_values = np.array( [[2.0, 2.2, 2.4, 2.6],
                          [4.0, 4.2, 4.4, 4.6],
                          [6.0, 6.2, 6.4, 6.6],
                          [8.0, 8.2, 8.4, 8.6]] )
    surface = BezierSurface( z_values )
    surface.print_control_points()

if __name__ == '__main__':
    main()
