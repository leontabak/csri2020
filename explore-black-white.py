
import argparse

import math
from PIL import Image
from PIL import ImageColor


def weighted_average( a, b, t ):
    return (1 - t) * a + t * b

def weighted_average_points( a, b, t ):
    (x0, y0) = a
    (x1, y1) = b
    x = weighted_average( x0, x1, t )
    y = weighted_average( y0, y1, t )
    return (x, y)

def weighted_average_colors( a, b, t ):
    (a_red, a_green, a_blue) = a
    (b_red, b_green, b_blue) = b

    red = int( weighted_average( a_red, b_red, t ) )
    green = int( weighted_average( a_green, b_green, t ) )
    blue = int( weighted_average( a_blue, b_blue, t ) )

    return (red, green, blue )

def make_spectrum( a, b, number_of_colors ):
    spectrum = []

    for i in range( number_of_colors ):
        t = i / (number_of_colors - 1)
        color = weighted_average_colors( a, b, t )
        spectrum.append( color )

    return spectrum

def compute_shade_posterization( color, spectrum ):
    (red, green, blue) = color

    (red_weight, green_weight, blue_weight) = (0.30, 0.59, 0.11)

    average = int(red_weight * red + 
        green_weight * green + blue_weight * blue)

    number_of_colors = len( spectrum )

    index = average // (256 // number_of_colors)

    return spectrum[ index ]

def compute_shade_arithmetic_mean( color ):
    (red, green, blue) = color

    average = (red + green + blue) // 3

    return (average, average, average)
# compute_shade_arithmetic_mean()

def compute_shade_min_max_mean( color ):
    (red, green, blue) = color

    minimum = min( red, green, blue )
    maximum = max( red, green, blue )

    average = (minimum + maximum) // 2

    return (average, average, average)
# end of computer_shade_min_max_mean()

def compute_shade_weighted_average( color, weights ):
    (red, green, blue) = color
    (red_weight, green_weight, blue_weight) = weights

    average = int(red_weight * red + green_weight * green + blue_weight * blue)

    return (average, average, average)
# end of compute_shade_weighted_average()

def fun_0( color, p ):
    (red, green, blue) = color
    luminosity = 0.30 * red + 0.59 * green + 0.11 * blue

    t = luminosity / 255

    if t < 0.4:
        return (0, 0, 0)
    elif t > 1.0:
        return (255, 255, 255)
    else:
        p0 = weighted_average_points( (0,0), p, t )
        p1 = weighted_average_points( (1,1), p, t )
        p2 = weighted_average_points( p0, p1, t )

        r = int( p2[0] * red )
        g = int( p2[0] * green )
        b = int( p2[0] * blue )

        return (r, g, b)
# end of fun_0()

def approximately_equal( a, b ):
    EPSILON = 1E-8
    return abs( a - b ) < EPSILON

def fun_1( color ):

    # translate from rgb to hsv

    (red, green, blue) = color

    red_normalized = red / 255
    green_normalized = green / 255
    blue_normalized = blue / 255

    color_max = max( red_normalized, green_normalized, 
        blue_normalized )

    color_min = min( red_normalized, green_normalized, 
        blue_normalized )

    delta = color_max - color_min

    hue = 0
    if approximately_equal( delta, 0.0 ):
        hue = 0.0
    elif approximately_equal( color_max, red_normalized ):
        hue = (green_normalized - blue_normalized) / delta
    elif approximately_equal( color_max, green_normalized ):
        hue = 2  + (blue_normalized - red_normalized) / delta
    elif approximately_equal( color_max, blue_normalized ):
        hue = 4 + (red_normalized - green_normalized) / delta
    else:
        print( "1. Should never get here!" )

    hue = hue * 60
    if hue < 0:
        hue = hue + 360

    saturation = 0.0
    if color_max > 0.0:
        saturation = delta / color_max

    value = color_max

    # change saturation

    if 70 < hue < 170:
        saturation = (saturation + 1.0) / 2

    if value < 0.4:
        value = 0.0

    # translate from hsv to rgb

    c = value * saturation

    x = c * (1 - abs((hue / 60) % 2 -1))

    m  = value - c

    (r_prime, g_prime, b_prime) = (0, 0, 0)
    if 0 <= hue < 60:
        (r_prime, g_prime, b_prime) = (c, x, 0) 
    elif 60 <= hue < 120:
        (r_prime, g_prime, b_prime) = (x, c, 0)
    elif 120 <= hue < 180:
        (r_prime, g_prime, b_prime) = (0, c, x)
    elif 180 <= hue < 240:
        (r_prime, g_prime, b_prime) = (0, x, c)
    elif 240 <= hue < 300:
        (r_prime, g_prime, b_prime) = (x, 0, c)
    elif 300 <= hue < 360:
        (r_prime, g_prime, b_prime) = (c, 0, x)
    else:
        print( "2. Should never get here!" )

    red = int((r_prime + m) * 255)
    green = int((g_prime + m) * 255)
    blue = int((b_prime + m) * 255)

    return (red, green, blue)

# end of fun_1()

def choose_color( color, i, j, width, height, spectrum ):
    # TO-DO: experiment with other formulas
    # for deciding which pixels should be
    # drawn in color and which should be 
    # drawn in black & white

    # TO-DO experiment with each of the
    # following color-to-share-of-gray algorithms

    
    rule_width = 4
    white = (255, 255, 255)

    if abs(i - ((3 * width) // 4)) < rule_width:
        return white
    elif abs( i - ((2 * width) // 4)) < rule_width:
        return white
    elif abs( i - ((1 * width) // 4)) < rule_width:
        return white

    elif i >  ((3 * width) // 4):
        # convert color to shade of gray by
        # using the arithmetic mean algorithm    
        shade = compute_shade_arithmetic_mean( color )
        return shade
 
    elif i > ((2 * width) // 4):
        # convert color to shade of gray by
        # using the mean of the minimum and maximum
        # of the colors 3 components
        shade = compute_shade_min_max_mean( color )
        return shade

    elif i > ((1 * width) // 4):
        # convert color to shade of gray by
        # using the weighted average algorithm
        weights = (0.30, 0.59, 0.11)
        shade = compute_shade_weighted_average( color, 
            weights )
        return shade

    else:
        # do not convert color to a shade of gray
        return color
    

#    x_normalized = (width/2 - i) / (width/2)
#    y_normalized = (height/2 -j) / (height/2)
#
#    x_squared = x_normalized * x_normalized
#    y_squared = y_normalized * y_normalized

#    radius = math.sqrt( x_squared + y_squared )

#    if radius < 0.6:
#        color = fun_1( color )
#    else:
#        color = weighted_average_colors( color, 
#            (40, 180, 60), radius )

    return color

# end of choose_color()

def color_to_black_and_white( photo ):
    (width, height) = photo.size

    copy = photo.copy()

    spectrum = make_spectrum( (144, 248, 160), 
      (112, 128, 248), 8 )

    for i in range( width ):
        for j in range( height ):
            color = photo.getpixel( (i, j) )

            color = choose_color( color, i, j, width, height, spectrum )

            copy.putpixel( (i, j), color )

    return copy
# end of color_to_black_and_white()


def main( arguments ):
    photo = Image.open( arguments.image_file_name )
    #photo.show()

    (width, height) = photo.size
    print( "width = ", width )
    print( "height = ", height )

    bw_photo = color_to_black_and_white( photo )

    bw_photo.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument( 'image_file_name', type = str, 
        help = 'Name of image file' )

    arguments = parser.parse_args()

    main( arguments )


