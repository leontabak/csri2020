
from PIL import Image

def main():
    image = Image.open( 'ink-pond.jpg' )
    (width, height) = image.size

    if width > height:
        ulx = (height - width) // 2
        uly = 0
        lrx = width - ulx
        lry = height
        image = image.crop( (ulx, uly, lrx, lry) )

        print( image.size )

    image.show()
    image.close()

if __name__ == '__main__':
    main()
