
import cornell_pix as cp

import argparse

from PIL import Image
from PIL import ImageOps

def main( arguments ):
    # TO-DO: Experiment with your own images.
    photo = Image.open( arguments.image_file_name )

    photo = cp.halve_size_of_image( cp.make_image_square( photo ) )

    photo.show()

    # Experiment with each of the available operations.
    #   operation = 0  produces a grayscale (black and white) image
    #   operation = 1  produces a negative image
    #   operation = 2  produces posterization
    #   operation = 3  produces solarization

    # The ImageOps class defines other methods.
    # You might want to experiment with those methods also.

    operation = arguments.operation

    if operation == 0:
        changed_photo = ImageOps.grayscale( photo )
        changed_photo.show()
    elif operation == 1:
        changed_photo = ImageOps.invert( photo )
        changed_photo.show()
    elif operation == 2:
        # value assigned to bits must be an integer 1 to 8
        changed_photo = ImageOps.posterize( photo, bits = 1 )
        changed_photo.show()
    elif operation == 3:
        # value assigned to threshold must be an integer 0 to 255
        changed_photo = ImageOps.solarize( photo, threshold = 64 )
        changed_photo.show()


    photo.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument( 'image_file_name', type = str, 
        help = 'Name of image file' )

    parser.add_argument( '--operation', type = int, default = 0,
        help = 'Index of operation' )

    arguments = parser.parse_args()

    main( arguments )

