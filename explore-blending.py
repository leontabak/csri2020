
import argparse
import math

from PIL import Image
from PIL import ImageOps

# define functions that compute weighted averages

def weighted_average( a, b, t ):
    """ 
        Compute the weighted average of 2 numbers. 

        param: a is one of the numbers to be averaged
        param: b is the other number to be averaged
        param: t is the weight, 0.0 <= t <= 1.0
        return: the weighted average of a and b, with weight t
    """
    return (1 - t) * a + t * b
# end of weighted_average()

def weighted_average_points( a, b, t ):
    """ Compute the weighted average of 2 points. """
    (x0, y0) = a
    (x1, y1) = b
    x = weighted_average( x0, x1, t )
    y = weighted_average( y0, y1, t )
    return (x, y)
# end of weighted_average_points()

def weighted_average_colors( a, b, t ):
    """ Compute the weighted average of 2 colors. """
    (a_red, a_green, a_blue) = a
    (b_red, b_green, b_blue) = b

    red = int( weighted_average( a_red, b_red, t ) )
    green = int( weighted_average( a_green, b_green, t ) )
    blue = int( weighted_average( a_blue, b_blue, t ) )

    return (red, green, blue)
# end of weighted_average_colors()

# define functions that compute normalized coordinates

def normalized_cartesian_coordinates( row, column, width, height ):
    """ Compute x and y coordinates where -1.0 <= x, y <= 1.0. """
    half_width = width / 2
    half_height = height / 2

    radius = max( half_width, half_height )

    x = (column - half_width) / radius
    y = (row - half_height) / radius

    return (x, y)
# end of normalized_cartesian_coordinates()

def normalized_polar_coordinates( row, column, width, height ):
    # compute polar coordinates: a radius and an angle
    #    0.0 <= radius <= 1.0
    #    0.0 <= angle < 2 * pi
    # radius is a measure of the distance of a pixel
    # from the center of the image
    (x, y) = normalized_cartesian_coordinates( row, column, 
        width, height )

    radius = math.sqrt( x * x + y * y )
    angle = math.atan2( y, x )

    return (radius, angle )
# end of normalized_polar_coordinates()

# define functions that change dimensions of an image

def make_image_square( image ):
    # get the current dimensions of the image
    (width, height) = image.size

    # crop to get a square image
    if width > height:
        # compute number of pixels to be cut off
        # at left and right of image
        margin = (width - height) // 2

        # coordinates of upper left corner
        # of region that will be retained
        ulx = margin
        uly = 0

        # coordinates of lower right corner
        # of region that will be retained
        lrx = width - margin
        lry = height
        image = image.crop( (ulx, uly, lrx, lry) )
    else:
        # compute number of pixels to be cut off
        # at top and bottom of image
        margin = (height - width) // 2

        # coordinates of upper left corner
        # of region that will be retained
        ulx = 0
        uly = margin

        # coordinates of lower right corner
        # of region that will be retained
        lrx = width
        lry = height - margin
        image = image.crop( (ulx, uly, lrx, lry) )

    return image
# end of make_image_square()

def halve_size_of_image( image ):
    (width, height) = image.size
    width = width // 2
    height = height // 2

    image = ImageOps.fit( image, (width, height) )

    return image
# end of halve_size_of_image()

def main( arguments ):
    photo = Image.open( arguments.image_file_name )

    # crop and scale image
    photo = make_image_square( photo )
    photo = halve_size_of_image( photo )

    # produce copies of image

    # leave one copy unchanged
    # TO-DO: experment changes to this copy
    color_photo = photo.copy()

    # change the other copy to black and white
    # TO-DO: experiment with other changes
    grayscale_photo = photo.copy()
    grayscale_photo = ImageOps.grayscale( grayscale_photo )
    grayscale_photo = grayscale_photo.convert( mode = 'RGB' )


    (width, height) = photo.size

    # scan image left to right and top to bottom
    # compute color of each pixel in new image
    for column in range( 0, width ):
        for row in range( 0, height ):

            (radius, angle) = normalized_polar_coordinates( row, column,
                width, height )

            if radius < arguments.radius:
                # color pixels near the center of the image in one way
                c = color_photo.getpixel( (column, row) )
                photo.putpixel( (column, row), c )
            else:
                # color pixels farther from the center of the image another way

                # get the color of a pixel from one image
                a = color_photo.getpixel( (column, row) )

                # get the color of a pixel from another image
                b = grayscale_photo.getpixel( (column, row) )

                # compute a weight to use in computing weighted
                # average of the two colors: 0.0 <= t <= 1.0
                t = (radius - arguments.radius) / (1.0 - arguments.radius)

                # TO-DO: experiment with different powers
                # here to control the speed of the transition
                # from one image to another
                power = 1.0
                t = math.pow( t, power )

                c = weighted_average_colors( a, b, t )
                photo.putpixel( (column, row), c )

    photo.show()
    photo.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser( description = 'Example' )

    # define the command line arguments

    # image_file_name is a positional command line argument
    parser.add_argument( 'image_file_name', type = str,
        help = 'Provide the name of an image file.' )

    # image_file_name is an optional command line argument
    parser.add_argument( '--radius', default = 0.5, type = float,
        help = 'Provide a radius: 0.0 <= radius <= 1.0.' )

    arguments = parser.parse_args()

    main( arguments )
