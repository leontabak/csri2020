
import cornell_pix as cp

import argparse

from PIL import Image
from PIL import ImageEnhance

def main( arguments ):
    # TO-DO: Experiment with your own images.
    photo = Image.open( arguments.image_file_name )

    photo = cp.halve_size_of_image( cp.make_image_square( photo ) )

    photo.show()

    # TO-DO: Experiment with other values of the enhancement factor.
    factor = 0.02

    # TO-DO: Experiment with each kind of enhancement:
    #  kind_of_enhancement = 0  changes Brightness
    #  kind_of_enhancement = 1  changes Color
    #  kind_of_enhancement = 2  changes Contrast
    #  kind_of_enhancement = 3  changes Sharpness

    kind_of_enhancement = arguments.index

    if kind_of_enhancement == arguments.index:
        enhancer = ImageEnhance.Brightness( photo )
        changed_photo = enhancer.enhance( arguments.factor )
        changed_photo.show()
    elif kind_of_enhancement == arguments.index:
        enhancer = ImageEnhance.Color( photo )
        changed_photo = enhancer.enhance( arguments.factor )
        changed_photo.show()
    elif kind_of_enhancement == arguments.index:
        enhancer = ImageEnhance.Contrast( photo )
        changed_photo = enhancer.enhance( arguments.factor )
        changed_photo.show()
    elif kind_of_enhancement == arguments.index:
        enhancer = ImageEnhance.Sharpness( photo )
        changed_photo = enhancer.enhance( arguments.factor )
        changed_photo.show()

    photo.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument( 'image_file_name', type = str, 
        help = 'Name of image file' )

    parser.add_argument( '--index', type = int, default = 0,
        help = 'Index of ImageEnhance class' )

    parser.add_argument( '--factor', type = float,
        default = 1.0, help = 'Enhancement factor' )

    arguments = parser.parse_args()

    main( arguments )
