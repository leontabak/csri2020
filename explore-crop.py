
import argparse

from PIL import Image
from PIL import ImageFilter

def main( arguments ):
    photo = Image.open( arguments.image_file_name )

    (width, height) = photo.size

    x_min = 0
    y_min = 0
    x_max = width
    y_max = height // 4
    top = photo.crop( (x_min, y_min, x_max, y_max) )

    x_min = 0
    y_min = 3 * height // 4
    x_max = width
    y_max = height
    bottom = photo.crop( (x_min, y_min, x_max, y_max) )

    x_min = 0
    y_min = height // 4
    x_max = width // 4
    y_max = 3 * height // 4
    left = photo.crop( (x_min, y_min, x_max, y_max) )

    x_min = 3 * width // 4
    y_min = height // 4
    x_max = width
    y_max = 3 * height // 4
    right = photo.crop( (x_min, y_min, x_max, y_max) )


    top = top.filter( ImageFilter.CONTOUR )
    bottom = bottom.filter( ImageFilter.CONTOUR )
    left = left.filter( ImageFilter.CONTOUR )
    right = right.filter( ImageFilter.CONTOUR )

    x_min = 0
    y_min = 0
    photo.paste( top, box = (x_min, y_min) )

    x_min = 0
    y_min = 3 * height // 4
    photo.paste( bottom, box = (x_min, y_min) )

    x_min = 0
    y_min = height // 4
    photo.paste( left, box = (x_min, y_min) )

    x_min = 3 * width // 4
    y_min = height // 4
    photo.paste( right, box = (x_min, y_min) )

    photo.show()

    photo.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument( 'image_file_name', type = str, 
        help = 'Name of image file' )

    arguments = parser.parse_args()

    main( arguments )
