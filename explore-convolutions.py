
import argparse

from PIL import Image
from PIL import ImageFilter
from PIL import ImageOps

def make_image_square( image ):
    (width, height) = image.size

    # crop to get a square image
    if width > height:
        margin = (width - height) // 2
        ulx = margin
        uly = 0
        lrx = width - margin
        lry = height
        image = image.crop( (ulx, uly, lrx, lry) )
    else:
        margin = (height - width) // 2
        ulx = 0
        uly = margin
        lrx = width
        lry = height - margin
        image = image.crop( (ulx, uly, lrx, lry) )

    (width, height) = image.size

    image = ImageOps.fit( image, (width, height) )

    return image

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument( 'image_file', type = str, help = 'Image file' )

    parser.add_argument( '--kernel_index', type = int, default = 0,
        help = 'Integer index to specify kernel for convolution filter' )

    args = parser.parse_args()

    image = Image.open( args.image_file )

    image = make_image_square( image )

    (width, height) = image.size

    width = width // 2
    height = height // 2
    image = ImageOps.fit( image, (width, height) )


    # dimensions of kernel
    size = (3, 3)

    kernels = []

    # sharpen
    kernels.append( ( 0, -1, 0, -1, 5, -1, 0, -1, 0) )

    # outline
    kernels.append( ( -1, -1, -1, -1, 8, -1, -1, -1, -1 ) )

    # blur
    kernels.append( (0.0625, 0.125, 0.0625, 0.125, 0.250, 0.125,
        0.0625, 0.125, 0.0625) )

    # emboss
    kernels.append( (-2, -1, 0, -1, 1, 1, 0, 1, 2) )

    kernel = kernels[ args.kernel_index ]
 
    image = image.filter( ImageFilter.Kernel( size, kernel, scale = 1 ) )


    image.show()
    image.close()

if __name__ == '__main__':
    main()
