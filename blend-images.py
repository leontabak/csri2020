
import cornell_pix as cp
import math
import numpy as np

from PIL import Image
from PIL import ImageFilter
from PIL import ImageOps

class BezierCurve:
    def __init__(self, control_points):
        self.control_points = np.array( control_points )
        self.control_points.reshape( 4, 2 )

        self.m = np.array( [[ 1,  0,  0,  0], 
                            [-3,  3,  0,  0], 
                            [ 3, -6,  3,  0], 
                            [-1,  3, -3,  1]] )

    def normalized_point(self, t):
        t_squared = t * t
        t_cubed = t * t_squared
        t_vector = np.array( [1, t, t_squared, t_cubed] )
        t_vector = t_vector.reshape( 1, 4 )

        row_vector = t_vector.dot( self.m )

        point = row_vector.dot( self.control_points )

        return Point( point[0,0], point[0,1] )

    def point(self, column, width, height):
        t = column / width
        p = self.normalized_point( t )

        return Point(int( p.x * width ), int( p.y * height ))

    def distance(self, row, column, width, height):
        point_on_curve = self.point( column, width, height )
        y = point_on_curve.y

        if row > y:
            return (row - y) / (height - y)
        else:
            return (row - y) / y

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

def test_normalized_point( curve, number_of_points ):
    for t in np.linspace( 0.0, 1.0, number_of_points, endpoint = True ):
        p = curve.normalized_point( t )
        print ('t = {:8.4f} point = ({:8.4f}, {:8.4f})'.format( t, p.x, p.y ) )

def test_point( curve, width, height ):
    for column in range(0, width + 1):
        p = curve.point( column, width, height )
        print( '({:4d}, {:4d})'.format( p.x, p.y ) )         

def main():
    # TO-DO: Experiment with different control points.
    #
    #     Choose coordinates subject to these constraints:
    #
    #     0.0 <= x0, x1, x2, x3 <= 1.0
    #     0.0 <= y0, y1, y2, y3 <= 1.0
    #
    #     x0 = 0.0
    #     x3 = 1.0

    x0 = 0.0
    y0  =0.2

    x1 = 0.4
    y1 = 0.8

    x2 = 0.6
    y2 = 0.4

    x3 = 1.0
    y3 = 0.6

    control_points = [[x0, y0], [x1, y1], [x2, y2], [x3, y3]]
    curve = BezierCurve( control_points )

    #number_of_points = 20
    #test_normalized_point( curve, number_of_points )

    #width = 64
    #height = 48
    #test_point( curve, width, height )    

    # TO-DO: Experiment with your own images.
    image = Image.open( 'old-sem.jpg' )
    (width, height) = image.size

    print( 'Dimensions before cropping: {:4d} x {:4d} pixels'.format(
        width, height ) )

    # crop to get a square image
    if width > height:
        margin = (width - height) // 2
        ulx = margin
        uly = 0
        lrx = width - margin
        lry = height
        image = image.crop( (ulx, uly, lrx, lry) )
    else:
        margin = (height - width) // 2
        ulx = 0
        uly = margin
        lrx = width
        lry = height - margin
        image = image.crop( (ulx, uly, lrx, lry) )

    (width, height) = image.size

    print( 'Dimensions after cropping: {:4d} x {:4d} pixels'.format(
        width, height ) )

    image = ImageOps.fit( image, (width // 2, height // 2) )

    (width, height) = image.size

    print( 'Dimensions after fitting: {:4d} x {:4d} pixels'.format(
        width, height ) )

    # TO-DO: Experiment with different modifications of the image.
    image_a = image.copy()
    image_a = image_a.filter( ImageFilter.CONTOUR )

    image_b = image.copy()
    image_b = ImageOps.grayscale( image )
    #image_b = ImageOps.invert( image_b )
    image_b = image_b.convert( 'RGB' )

    for row in range( 0, height ):
        for column in range( 0, width ):
            t = curve.distance( row, column, width, height )

            color_a = image_a.getpixel( (column, row) )
            color_b = image_b.getpixel( (column, row) )

            # TO-DO: Experiment with different values of power.
            power = 2.0

            if t > 0:
                # Do one thing for pixels above the curve.
                t = math.pow( t, power )
                # TO-DO: Experiment with t = t vs. t = 1.0 - t
                # TO-DO: Experiment with t = 0.0 and t = 1.0
                t = t
                # t = 1.0 - t
                #t = 1.0
                color_mix = cp.weighted_average_colors( color_a, color_b, t )
            else:
                # Do something else for pixels below the curve.
                t = -t
                t = 1.0 - math.pow( t, power )
                # TO-DO: Experiment with t = t vs. t = 1.0 - t
                # TO-DO: Experiment with t = 0.0 and t = 1.0
                t = t
                # t = 1.0 - t
                #t = 0.0
                color_mix = cp.weighted_average_colors( color_a, color_b, t )

            image.putpixel( (column, row), color_mix )

    image.show()
    image.close()

if __name__ == '__main__':
    main()
