import math
import numpy as np

from PIL import Image
from PIL import ImageDraw
from PIL import ImageOps

import random

class BezierCurve:

    # a point on a Bezier curve can be computed by 
    # multiplying 3 matrices:
    #   (1) a matrix with 1 row and 4 columns
    #       The elements of this matrix are powers of a 
    #       weight t (0.0 <= t <= 1.0).
    #       The value of t determines which point on the curve 
    #       this function returns.
    #   (2) a matrix with 4 rows and 4 columns
    #       The elements of this matrix are always the same.
    #   (3) a matrix with 4 rows and 2 columns
    #       The elements of this matrix are the coordinates
    #       of 4 controls points.
    #       The choice of control points determines the shape
    #       of the curve.

    # A Bezier curve begins at the first control point and ends
    # at the fourth control point.
    # In general, it will not pass through the second and third
    # control points but will lie within a polygon (the "convex 
    # hull") defined by all 4 control points.

    def __init__(self, control_points):
        self.control_points = np.array( control_points )
        self.control_points.reshape( 4, 2 )

        self.m = np.array( [[ 1,  0,  0,  0], 
                            [-3,  3,  0,  0], 
                            [ 3, -6,  3,  0], 
                            [-1,  3, -3,  1]] )

    def normalized_point(self, t):
        t_squared = t * t
        t_cubed = t * t_squared
        t_vector = np.array( [1, t, t_squared, t_cubed] )
        t_vector = t_vector.reshape( 1, 4 )

        row_vector = t_vector.dot( self.m )

        point = row_vector.dot( self.control_points )

        return Point( point[0,0], point[0,1] )

    def point(self, column, width, height):
        t = column / width

        p = self.normalized_point( t )

        return Point(int( p.x * width ), int( p.y * height ))

    def distance(self, row, column, width, height):
        point_on_curve = self.point( column, width, height )
        y = point_on_curve.y

        if row > y:
            return (row - y) / (height - y)
        else:
            return (row - y) / y
# end of BezierCurve class

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
# end of Point class


def approximately_equal( a, b ):
    """
       Return True if a and b are nearly equal, False otherwise. 

       The computer represents floating point values with finite
       precision, and so computations that produce the same 
       mathematical result (e.g., the square root of 1/4 and the
       sine of 30 degrees) are not guaranteed to give identical
       floating point values.

       param: a is one of the two numbers to be compared
       param: b is the other of the two numbers to be compared
       return: True if the difference between a and b is very
           small, False otherwise
    """

    # 1E-8 is one hundred millionth
    # this is a small, arbitrary constant
    # if difference between 2 numbers is smaller
    # than this, call the numbers equal
    EPSILON = 1E-8
    return abs( a - b ) < EPSILON
# end of approximately_equal()

# compute weighted averages

def weighted_average( a, b, t ):
    """ 
        Compute the weighted average of 2 numbers. 

        param: a is one of the numbers to be averaged
        param: b is the other number to be averaged
        param: t is the weight, 0.0 <= t <= 1.0
        return: the weighted average of a and b, with weight t
    """
    return (1 - t) * a + t * b
# end of weighted_average()

def weighted_average_points( a, b, t ):
    """ Compute the weighted average of 2 points. """
    (x0, y0) = a
    (x1, y1) = b
    x = weighted_average( x0, x1, t )
    y = weighted_average( y0, y1, t )
    return (x, y)
# end of weighted_average_points()

def weighted_average_colors( a, b, t ):
    """ Compute the weighted average of 2 colors. """
    (a_red, a_green, a_blue) = a
    (b_red, b_green, b_blue) = b

    red = int( weighted_average( a_red, b_red, t ) )
    green = int( weighted_average( a_green, b_green, t ) )
    blue = int( weighted_average( a_blue, b_blue, t ) )

    return (red, green, blue )
# end of weighted_average_colors()

def rgb_to_hsv( color ):
    """ Translate from rgb to hsv. """

    (red, green, blue) = color

    red_normalized = red / 255
    green_normalized = green / 255
    blue_normalized = blue / 255

    color_max = max( red_normalized, green_normalized, 
        blue_normalized )

    color_min = min( red_normalized, green_normalized, 
        blue_normalized )

    delta = color_max - color_min

    hue = 0
    if approximately_equal( delta, 0.0 ):
        hue = 0.0
    elif approximately_equal( color_max, red_normalized ):
        hue = (green_normalized - blue_normalized) / delta
    elif approximately_equal( color_max, green_normalized ):
        hue = 2  + (blue_normalized - red_normalized) / delta
    elif approximately_equal( color_max, blue_normalized ):
        hue = 4 + (red_normalized - green_normalized) / delta
    else:
        print( "1. Should never get here!" )

    hue = hue * 60
    if hue < 0:
        hue = hue + 360

    saturation = 0.0
    if color_max > 0.0:
        saturation = delta / color_max

    value = color_max

    return (hue, saturation, value)

def hsv_to_rgb( color ):
    """ Translate from hsv to rgb. """
    (hue, saturation, value) = color

    c = value * saturation

    x = c * (1 - abs((hue / 60) % 2 - 1) )

    m  = value - c

    (r_prime, g_prime, b_prime) = (0, 0, 0)
    if 0 <= hue < 60:
        (r_prime, g_prime, b_prime) = (c, x, 0) 
    elif 60 <= hue < 120:
        (r_prime, g_prime, b_prime) = (x, c, 0)
    elif 120 <= hue < 180:
        (r_prime, g_prime, b_prime) = (0, c, x)
    elif 180 <= hue < 240:
        (r_prime, g_prime, b_prime) = (0, x, c)
    elif 240 <= hue < 300:
        (r_prime, g_prime, b_prime) = (x, 0, c)
    elif 300 <= hue < 360:
        (r_prime, g_prime, b_prime) = (c, 0, x)
    else:
        print( "2. Should never get here!" )

    red = int((r_prime + m) * 255)
    green = int((g_prime + m) * 255)
    blue = int((b_prime + m) * 255)

    return (red, green, blue)

def make_spectrum( a, b, number_of_colors ):
    """ Create a list of evenly spaced colors. """
    spectrum = []

    for i in range( number_of_colors ):
        t = i / (number_of_colors - 1)
        color = weighted_average_colors( a, b, t )
        spectrum.append( color )

    return spectrum
# end of make_spectrum()

# compute normalized coordinates

def normalized_cartesian_coordinates( row, column, width, height ):
    """ Compute x and y coordinates where -1.0 <= x, y <= 1.0. """
    half_width = width / 2
    half_height = height / 2

    radius = max( half_width, half_height )

    x = (column - half_width) / radius
    y = (row - half_height) / radius

    return (x, y)
# end of normalized_cartesian_coordinates()

def normalized_polar_coordinates( row, column, width, height ):
    (x, y) = normalized_cartesian_coordinates( row, column, 
        width, height )

    radius = math.sqrt( x * x + y * y )
    angle = math.atan2( y, x )

    return (radius, angle )
# end of normalized_polar_coordinates()

# convert an rgb color to a shade of gray
# (each function uses a different algorithm)

def compute_shade_arithmetic_mean( color ):
    """ Compute a shade of gray that corresponds to a given color. """
    (red, green, blue) = color

    average = (red + green + blue) // 3

    return (average, average, average)
# compute_shade_arithmetic_mean()

def compute_shade_min_max_mean( color ):
    """ Compute a shade of gray that corresponds to a given color. """
    (red, green, blue) = color

    minimum = min( red, green, blue )
    maximum = max( red, green, blue )

    average = (minimum + maximum) // 2

    return (average, average, average)
# end of computer_shade_min_max_mean()

def compute_shade_weighted_average( color, weights ):
    """ Compute a shade of gray that corresponds to a given color. """
    (red, green, blue) = color
    (red_weight, green_weight, blue_weight) = weights

    sum_of_products = 0.0
    sum_of_products += red_weight * red
    sum_of_products += green_weight * green
    sum_of_products += blue_weight * blue
  
    average = int( sum_of_products )

    return (average, average, average)
# end of compute_shade_weighted_average()

def compute_shade_luminosity( color ):
    """ Compute a shade of gray that corresponds to a given color. """
    weights = (0.30, 0.59, 0.11)
    shade_of_gray = compute_shade_weighted_average( color, weights )
    return shade_of_gray 
# end of compute_shade_luminosity()

def compute_bw_negative( color ):
    """ Compute the shade for a b & w negative image. """
    shade = compute_shade_luminosity( color )

    # a shade of gray is an rgb color in which
    # red = green = blue
    # each component of the color is an integer in
    # the range [0, 255]

    return 255 - shade[0]
# end of compute_bw_negative()

def compute_color_negative( color ):
    """ Compute the color for a color negative image. """
    (red, green, blue) = color

    return (255 - red, 255 - green, 255 - blue)
# end of computer_color_negative()

def compute_shade_posterization( color, spectrum ):
    shade_of_gray = compute_shade_luminosity( color )

    number_of_colors = len( spectrum )

    # the values of all 3 components of shade_of_gray are equal
    # shade_of_gray[0] = shade_of_gray[1] = shade_of_gray[2]
    # so, the choice of shade_of_gray[0] is arbitrary
    luminosity = shade_of_gray[0]

    # 256 is the number of possible values for the
    # red, green, or blue components of an rgb color
    index =  luminosity // (256 // number_of_colors)

    return spectrum[ index ]
# end of compute_shade_posterization()

def make_image_square( image ):
    (width, height) = image.size

    # crop to get a square image
    if width > height:
        margin = (width - height) // 2
        ulx = margin
        uly = 0
        lrx = width - margin
        lry = height
        image = image.crop( (ulx, uly, lrx, lry) )
    else:
        margin = (height - width) // 2
        ulx = 0
        uly = margin
        lrx = width
        lry = height - margin
        image = image.crop( (ulx, uly, lrx, lry) )

    (width, height) = image.size

    image = ImageOps.fit( image, (width, height) )

    return image
# end of make_image_square()

def make_golden_rectangle( image ):
    tau = (math.sqrt( 5.0 )  + 1.0) / 2

    (width, height) = image.size

    # crop to get a square image
    if width > height:

        if tau * height > width:
            adjusted_height = width // tau
            v_margin  = (height - adjusted_height) // 2

            ulx = 0
            uly = v_margin

            lrx = width
            lry = height - v_margin

        else:
            adjusted_width = height * tau
            h_margin = (width - adjusted_width) // 2

            ulx = h_margin
            uly = 0

            lrx = width - h_margin
            lry = height
            
    else:
        if tau * width > height:
            adjusted_width = height // tau
            h_margin = (width - adjusted_width) // 2

            ulx = h_margin
            uly = 0

            lrx = width - h_margin
            uly = height

        else:
            adjusted_height = width * tau
            v_margin = (height - adjusted_height) // 2

            ulx = 0
            uly = v_margin

            lrx = width
            lry = height - v_margin

    image = image.crop( (ulx, uly, lrx, lry) )
    (width, height) = image.size

    image = ImageOps.fit( image, (width, height) )

    return image

# end of make_golden_rectangle()

def halve_size_of_image( image ):
    (width, height) = image.size

    width = width // 2
    height = height // 2
    image = ImageOps.fit( image, (width, height) )

    return image
# end of halve_size_of_image()

def map_endpoints( normalized_tail, normalized_head, 
        image_width, image_height ):

    x0 = normalized_tail.x
    y0 = normalized_tail.y

    x1 = normalized_head.x
    y1 = normalized_head.y

    x0 = int( x0 * image_width )
    y0 = int( y0 * image_height )
    x1 = int( x1 * image_width )
    y1 = int( y1 * image_height )

    return (x0, y0, x1, y1)
# end of map_endpoints()

def draw_curve( image, control_points ):
    curve = BezierCurve( control_points )

    (image_width, image_height) = image.size

    draw = ImageDraw.Draw( image )

    # specify width of line in pixels
    line_width = 8

    # specify color of line
    line_color = (255, 255, 0)

    steps = 512
    previous_point = curve.normalized_point( 0.0 )
    for n in range( 1, steps +  1):
        t = n / steps
        current_point = curve.normalized_point( t  )

        endpoints = map_endpoints( previous_point, current_point, 
            image_width, image_height )

        draw.line( endpoints, width = line_width, fill = line_color )

        previous_point = current_point
# end of draw_curve()
