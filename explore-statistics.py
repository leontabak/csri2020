
from PIL import Image
from PIL import ImageStat


def main():
    photo = Image.open( 'images/lilac-1024x768.jpg' )
    statistics = ImageStat.Stat( photo )

    print( 'Mode = ', photo.mode )

    print( '\n' )

    print( 'File format  = ', photo.format )

    print( '\n' )

    (width, height) = photo.size
    print( 'File size = {:4d} pixels x {:4d} pixels'.format( width, height ) )

    print( '\n' )

    print( 'extrema = ', statistics.extrema )
    (red_extrema, green_extrema, blue_extrema) = statistics.extrema
    print( 'minimum red value   = {:4d}'.format( red_extrema[0] ) )
    print( 'maximum red value   = {:4d}'.format( red_extrema[1] ) )

    print( '\n' )

    print( 'minimum green value = {:4d}'.format( green_extrema[0] ) )
    print( 'maximum green value = {:4d}'.format( green_extrema[1] ) )

    print( '\n' )

    print( 'minimum blue value  = {:4d}'.format( blue_extrema[0] ) )
    print( 'maximum blue value  = {:4d}'.format( blue_extrema[1] ) )

    print( '\n' )

    print( 'mean red value   = {:8.2f}'.format( statistics.mean[0] ) )
    print( 'mean green value = {:8.2f}'.format( statistics.mean[1] ) )
    print( 'mean blue value  = {:8.2f}'.format( statistics.mean[2] ) )

    print( '\n' )

    print( 'median red value   = {:8.2f}'.format( statistics.median[0] ) )
    print( 'median green value = {:8.2f}'.format( statistics.median[1] ) )
    print( 'median blue value  = {:8.2f}'.format( statistics.median[2] ) )

    print( '\n' )

    print( 'standard deviation of red values   = {:8.2f}'.format( 
        statistics.stddev[0] ) )
    print( 'standard deviation of green values = {:8.2f}'.format( 
        statistics.stddev[1] ) )
    print( 'standard deviation of blue values  = {:8.2f}'.format( 
        statistics.stddev[2] ) )

    print( '\n' )

    photo.show()


if __name__ == '__main__':
    main()
