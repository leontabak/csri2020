
# csri2020.py
# 29 May 2020
# Cornell Summer Research Institute
# Robin Nilsson, Leon Tabak, Dylan Terrell, Drak Twining
# 

import math
from PIL import Image
import random

def approximately_equal( a, b ):
    """
       Return True if a and b are nearly equal, False otherwise. 

       The computer represents floating point values with finite
       precision, and so computations that produce the same 
       mathematical result (e.g., the square root of 1/4 and the
       sine of 30 degrees) are not guaranteed to give identical
       floating point values.

       param: a is one of the two numbers to be compared
       param: b is the other of the two numbers to be compared
       return: True if the difference between a and b is very
           small, False otherwise
    """

    # 1E-8 is one hundred millionth
    # this is a small, arbitrary constant
    # if difference between 2 numbers is smaller
    # than this, call the numbers equal
    EPSILON = 1E-8
    return abs( a - b ) < EPSILON
# end of approximately_equal()

# compute weighted averages

def weighted_average( a, b, t ):
    """ 
        Compute the weighted average of 2 numbers. 

        param: a is one of the numbers to be averaged
        param: b is the other number to be averaged
        param: t is the weight, 0.0 <= t <= 1.0
        return: the weighted average of a and b, with weight t
    """
    return (1 - t) * a + t * b
# end of weighted_average()

def weighted_average_points( a, b, t ):
    """ Compute the weighted average of 2 points. """
    (x0, y0) = a
    (x1, y1) = b
    x = weighted_average( x0, x1, t )
    y = weighted_average( y0, y1, t )
    return (x, y)
# end of weighted_average_points()

def weighted_average_colors( a, b, t ):
    """ Compute the weighted average of 2 colors. """
    (a_red, a_green, a_blue) = a
    (b_red, b_green, b_blue) = b

    red = int( weighted_average( a_red, b_red, t ) )
    green = int( weighted_average( a_green, b_green, t ) )
    blue = int( weighted_average( a_blue, b_blue, t ) )

    return (red, green, blue )
# end of weighted_average_colors()

def rgb_to_hsv( color ):
    """ Translate from rgb to hsv. """

    (red, green, blue) = color

    red_normalized = red / 255
    green_normalized = green / 255
    blue_normalized = blue / 255

    color_max = max( red_normalized, green_normalized, 
        blue_normalized )

    color_min = min( red_normalized, green_normalized, 
        blue_normalized )

    delta = color_max - color_min

    hue = 0
    if approximately_equal( delta, 0.0 ):
        hue = 0.0
    elif approximately_equal( color_max, red_normalized ):
        hue = (green_normalized - blue_normalized) / delta
    elif approximately_equal( color_max, green_normalized ):
        hue = 2  + (blue_normalized - red_normalized) / delta
    elif approximately_equal( color_max, blue_normalized ):
        hue = 4 + (red_normalized - green_normalized) / delta
    else:
        print( "1. Should never get here!" )

    hue = hue * 60
    if hue < 0:
        hue = hue + 360

    saturation = 0.0
    if color_max > 0.0:
        saturation = delta / color_max

    value = color_max

    return (hue, saturation, value)

def hsv_to_rgb( color ):
    """ Translate from hsv to rgb. """
    (hue, saturation, value) = color

    c = value * saturation

    x = c * (1 - abs((hue / 60) % 2 - 1) )

    m  = value - c

    (r_prime, g_prime, b_prime) = (0, 0, 0)
    if 0 <= hue < 60:
        (r_prime, g_prime, b_prime) = (c, x, 0) 
    elif 60 <= hue < 120:
        (r_prime, g_prime, b_prime) = (x, c, 0)
    elif 120 <= hue < 180:
        (r_prime, g_prime, b_prime) = (0, c, x)
    elif 180 <= hue < 240:
        (r_prime, g_prime, b_prime) = (0, x, c)
    elif 240 <= hue < 300:
        (r_prime, g_prime, b_prime) = (x, 0, c)
    elif 300 <= hue < 360:
        (r_prime, g_prime, b_prime) = (c, 0, x)
    else:
        print( "2. Should never get here!" )

    red = int((r_prime + m) * 255)
    green = int((g_prime + m) * 255)
    blue = int((b_prime + m) * 255)

    return (red, green, blue)

def make_spectrum( a, b, number_of_colors ):
    """ Create a list of evenly spaced colors. """
    spectrum = []

    for i in range( number_of_colors ):
        t = i / (number_of_colors - 1)
        color = weighted_average_colors( a, b, t )
        spectrum.append( color )

    return spectrum
# end of make_spectrum()

# compute normalized coordinates

def normalized_cartesian_coordinates( row, column, width, height ):
    """ Compute x and y coordinates where -1.0 <= x, y <= 1.0. """
    half_width = width / 2
    half_height = height / 2

    radius = max( half_width, half_height )

    x = (column - half_width) / radius
    y = (row - half_height) / radius

    return (x, y)
# end of normalized_cartesian_coordinates()

def normalized_polar_coordinates( row, column, width, height ):
    (x, y) = normalized_cartesian_coordinates( row, column, 
        width, height )

    radius = math.sqrt( x * x + y * y )
    angle = math.atan2( y, x )

    return (radius, angle )
# end of normalized_polar_coordinates()

# convert an rgb color to a shade of gray
# (each function uses a different algorithm)

def compute_shade_arithmetic_mean( color ):
    """ Compute a shade of gray that corresponds to a given color. """
    (red, green, blue) = color

    average = (red + green + blue) // 3

    return (average, average, average)
# compute_shade_arithmetic_mean()

def compute_shade_min_max_mean( color ):
    """ Compute a shade of gray that corresponds to a given color. """
    (red, green, blue) = color

    minimum = min( red, green, blue )
    maximum = max( red, green, blue )

    average = (minimum + maximum) // 2

    return (average, average, average)
# end of computer_shade_min_max_mean()

def compute_shade_weighted_average( color, weights ):
    """ Compute a shade of gray that corresponds to a given color. """
    (red, green, blue) = color
    (red_weight, green_weight, blue_weight) = weights

    sum_of_products = 0.0
    sum_of_products += red_weight * red
    sum_of_products += green_weight * green
    sum_of_products += blue_weight * blue
  
    average = int( sum_of_products )

    return (average, average, average)
# end of compute_shade_weighted_average()

def compute_shade_luminosity( color ):
    """ Compute a shade of gray that corresponds to a given color. """
    weights = (0.30, 0.59, 0.11)
    shade_of_gray = compute_shade_weighted_average( color, weights )
    return shade_of_gray 
# end of compute_shade_luminosity()

def compute_bw_negative( color ):
    """ Compute the shade for a b & w negative image. """
    shade = compute_shade_luminosity( color )

    # a shade of gray is an rgb color in which
    # red = green = blue
    # each component of the color is an integer in
    # the range [0, 255]

    return 255 - shade[0]
# end of compute_bw_negative()

def compute_color_negative( color ):
    """ Compute the color for a color negative image. """
    (red, green, blue) = color

    return (255 - red, 255 - green, 255 - blue)
# end of computer_color_negative()

def compute_shade_posterization( color, spectrum ):
    shade_of_gray = compute_shade_luminosity( color )

    number_of_colors = len( spectrum )

    # the values of all 3 components of shade_of_gray are equal
    # shade_of_gray[0] = shade_of_gray[1] = shade_of_gray[2]
    # so, the choice of shade_of_gray[0] is arbitrary
    luminosity = shade_of_gray[0]

    # 256 is the number of possible values for the
    # red, green, or blue components of an rgb color
    index =  luminosity // (256 // number_of_colors)

    return spectrum[ index ]
# end of compute_shade_posterization()

def transform_pixel( color, row, column, width, height, spectrum ):

    # TODO: try other functions to change the color of a pixel.
    # You will find in this file definitions:
    #     - functions that produce a shade of gray
    #     - a "posterization function" that replaces
    #       a color with another color selected from a finite
    #       (and typically small) collection of colors
    #     - functions that produce negative images
    #     - a function that will blend two colors (or combine images)

    # In place of this call to compute_shade_luminosity(),
    # you may call any other function that returns an rgb color.

    (radius, angle) = normalized_polar_coordinates( row, column, 
        width, height )

    #color_in_changed_photo = color
    #if( radius > 0.6 ): 
    #    color_in_changed_photo =  compute_shade_luminosity( color )
    #    color_in_changed_photo = weighted_average_colors( color,
    #        color_in_changed_photo, 1.0 - (1.0 - radius) / 0.4 )

    white = (255, 255, 255)

    if random.random() > (radius * radius * radius):
        color_in_changed_photo = color
    else:
        color_in_changed_photo = compute_shade_luminosity( color )
        color_in_changed_photo = weighted_average_colors( white,
            color_in_changed_photo, 0.2 )
    
    # TODO: Use if / elif / else statements to determine
    # function is called to compute the color of the pixel
    # in the changed photo (or to determine the parameters
    # of the function). In this way, you can make one part
    # of the image color and other parts black and white,
    # or one part bright and another part dark, and so on.

    # For example, a circle or rectangle at the center of
    # of the image could be color while the rest of the 
    # image is black and white.

    return color_in_changed_photo

# end of transform_pixel()

def transform_photo( photo ):
    (width, height) = photo.size

    copy = photo.copy()

    # color at left end of spectrum
    # TODO: Change these 3 numbers as you wish.
    a = (144, 248, 160)
    # color at right end of spectrum
    # TODO: Change these 3 numbers as you wish.
    b = (112, 128, 248)
    # TODO: Change the number of colors in the
    # spectrum as you wish.
    number_of_colors = 8
    spectrum = make_spectrum( a, b, number_of_colors )

    for column in range( width ):
        for row in range( height ):
            color = photo.getpixel( (column, row) )

            color = transform_pixel( color, row, column, width, height, 
                spectrum )

            copy.putpixel( (column, row), color )

    return copy
# end of transform_photo()

def main():
    original_photo = Image.open( "lilac-1024x768.jpg" )
    original_photo.show()

    (width, height) = original_photo.size
    print( "width = ", width )
    print( "height = ", height )

    changed_photo = transform_photo( original_photo )

    changed_photo.show()

if __name__ == "__main__":
    main()
